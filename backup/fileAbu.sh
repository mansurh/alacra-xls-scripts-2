echo "File-A Backup Started on Data4" > logfile
date >> logfile

ntbackup backup "f:\\investext" /b /hc:on /t normal /l "logfile" /e /tape:0

echo "File-A Backup on Data4 Stopped" >> logfile
date >> logfile

# Mail Results of backup to Administrators group
blat "logfile" -t "administrators@xls.com" -s "File-A Backup on Data4 Log"
