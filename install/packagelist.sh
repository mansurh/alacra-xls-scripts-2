# Parse the command line arguments.
#	1 = directory
#	2 = email To address

shname=$0
if [ $# -lt 2 ]
then
    echo "Usage: ${shname} runpath emailto"
    exit 1
fi

runpath=$1
emailto=$2

if [ $# -lt 3 ]
then
	subj="List of Open Packages"
else
	subj="List of Open Packages in $3"
fi

tempfile=eraseme.dir
tempfile2=eraseme.dir2

cd ${runpath}
if [ $? -ne 0 ]
then
	echo "${shname}: error changing directory to ${runpath}, exiting ..."
	exit 1
fi


if [ ! -e ${tempfile} ]
then
	rm -f ${tempfile}
fi
if [ ! -e ${tempfile2} ]
then
	rm -f ${tempfile2}
fi

ls -l > ${tempfile}

awk -f $XLS/src/scripts/install/packages.awk ${tempfile} > ${tempfile2}

blat ${tempfile2} -t "${emailto}" -s "${subj}"

# rm -f ${tempfile}
# rm -f ${tempfile2}

exit 0
