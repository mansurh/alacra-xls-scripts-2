if [ "$XLSDATA" == "" ]
then
	echo "XLSDATA not defined, using c:/users/default"
	XLSDATA=c:/users/default
fi

logdir=$XLSDATA/install
if [ ! -d ${logdir} ]
then
	mkdir -p ${logdir}
fi

logfile=${logdir}/copywebinstallscripts.log

srcdir=c:/users/xls/src/scripts/install
dstdir=c:/usr/local/bin


signonmsg="Copying new web package install scripts on $COMPUTERNAME"
echo "$signonmsg" > ${logfile}

for a in				\
	${srcdir}/installpackage.sh		\
	${srcdir}/installpackagelow.sh		\
	${srcdir}/unpack.sh		\
	${srcdir}/qunpack.sh		\
	${srcdir}/unpackpackage.sh	\
	
do
	echo "Copying ${a} to ${dstdir}" >> ${logfile}
	cp -f ${a} ${dstdir}
	if [ $? != 0 ]
	then
		echo "Could not copy ${a} to ${dstdir}, Exiting" >> ${logfile}
		exit 1
	fi
done

blat ${logfile} -t "administrators@alacra.com" -s "$signonmsg" -f "administrators@alacra.com"
exit 0
