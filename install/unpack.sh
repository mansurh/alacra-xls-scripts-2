# Routine to unpack builds

shname=unpack.sh

# Parse the command line arguments
#	1 = date YYYYMMDD

if [ $# != 1 ]
then
	echo "Usage: ${shname} YYYYMMDD"
	exit 1
fi

# Save the runstring parameters in local variables
thedate=$1

distlistfile=distribution.web.${thedate}.lst
disttarzfile=distribution.web.${thedate}.tar.Z
fullpackname=distribution.web.${thedate}

undotarfile=undo.${thedate}.tar
undologfile=undo.${thedate}.log


# Make undo file
if [ ! -s ${distlistfile} ]
then
	echo "Distribution List file ${distlistfile} not found, exiting"
	exit 1
fi

if [ -s ${undotarfile} ]
then
	echo "Undo file ${undotarfile} already exists, exiting"
	exit 1
fi

if [ -s ${undotarfile}.gz ]
then
	echo "Undo file ${undotarfile}.gz already exists, exiting"
	exit 1
fi

echo "Creating undo tar file ${undotarfile}"
#read zz9
#echo "Creating undo tar file ${undotarfile}"
if [ "${OSTYPE}" = "cygwin" ]
then
  tar -cvf ${undotarfile} -T ${distlistfile}  > ${undologfile} 2>&1
else
  cat ${distlistfile} | tar -cvf ${undotarfile} -  > ${undologfile} 2>&1
fi
if [ ! -e ${undotarfile} ]
then
	echo "Undo tar file not created, exiting"
	exit 1
fi

echo "Compressing undo tar file ${undotarfile}"
#read zz9
#echo "Compressing undo tar file ${undotarfile}"
if [ "${OSTYPE}" = "cygwin" ]
then
  gzip ${undotarfile}
else
  mkszip ${undotarfile}
fi
if [ ! -e ${undotarfile}.gz ]
then
	echo "Undo tar file not compressed, exiting"
	exit 1
fi


echo "Unpacking distribution file ${disttarzfile}"
#read zz9
#echo "Unpacking distribution file ${disttarzfile}"
unpackpackage.sh ${fullpackname}
exit $?

