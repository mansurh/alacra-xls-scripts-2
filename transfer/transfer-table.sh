#parse the command line arguments
# 1 = table name
# 2 = sourcedb
# 3 = source server
# 4 = source login
# 5 = source passwd
# 6 = dest table
# 7 = dest db
# 8 = dest server
# 9 = dest user
# 10 = dest passwd

if [ $# -lt 10 ]
then
    echo "Usage: transfer-table.sh srctable srcdb srcserver srclogin srcpasswd desttable destdb destserver destlogin destpasswd"
    exit 1
fi

srctable=${1}
srcdb=${2}
srcserver=${3}
srcuser=${4}
srcpasswd=${5}
desttable=${6}
destdb=${7}
destserver=${8}
destuser=${9}
destpasswd=${10}

TEMPDIR=${XLSDATA}

rm -f ${TEMPDIR}/transfer.dat

echo "\nTransfer of ${srctable} table from ${srcdb} (${srcserver}) to ${desttable} table in ${destdb} (${destserver})"

bcp ${srcdb}.dbo.${srctable} out ${TEMPDIR}/transfer.dat /S${srcserver} /U${srcuser} /P${srcpasswd} /c /t"\001" /r"\002"
if [ $? != 0 ]
then
    echo "Error saving source data to file"
    exit 1
fi

${XLS}/bin/cleanuptable ${destserver} ${destuser} ${destpasswd} ${destdb} ${desttable} droptableindexes
if [ $? != 0 ]
then
    echo "Error dropping Destination indices"
    exit 1
fi

#echo "${XLS}/bin/cleanuptable ${destserver} ${destuser} ${destpasswd} ${destdb} ${desttable} truncatetable"
${XLS}/bin/cleanuptable ${destserver} ${destuser} ${destpasswd} ${destdb} ${desttable} truncatetable
if [ $? != 0 ]
then
    echo "Error truncating Destination table"
    exit 1
fi

echo "\nLoading data into destination..."

bcp ${destdb}.dbo.${desttable} in ${TEMPDIR}/transfer.dat /b10 /S${destserver} /U${destuser} /P${destpasswd} /c /t"\001" /r"\002"

# rebuild the destination indexes
