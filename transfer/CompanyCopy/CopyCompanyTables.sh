#parse the command line arguments
# 1 = source server
# 2 = source login
# 3 = source passwd
# 4 = dest server
# 5 = dest user
# 6 = dest passwd

shname=CopyCompanyTables.sh
if [ $# -lt 6 ]
then
    echo "Usage: ${shname} srcserver srclogin srcpasswd destserver destlogin destpasswd"
    exit 1
fi

srcserver=${1}
srcuser=${2}
srcpasswd=${3}
destserver=${4}
destuser=${5}
destpasswd=${6}

asdf=$(date +%Y%W%H%M%S)
tempfilename=CopyCompanyTables${asdf}.sql
temptablename=CopyCompanyTablesTemp${asdf}


tablelist="company snapshot1 security company_map security_map company_kwic co_sic co_naics co_industry co_gics company_source security_source exchange industry_xls gics_xls concept_xls country sic"
for tbl in $tablelist
do
	# make a temporary table with the same structure as the one we're going to copy
	$XLS/src/scripts/transfer/MakeEmptyTable.sh ${destserver} ${destuser} ${destpasswd} xls ${tbl} ${temptablename}
	if [ $? != 0 ]
	then
		echo "${shname}: Error in MakeEmptyTable.sh, exiting"
		exit 1
	fi

	# now copy the data into the temporary table
	query="select * from ${tbl}"
	$XLS/src/scripts/transfer/copyselectedrowslow.sh "${query}" xls ${srcserver} ${srcuser} ${srcpasswd} ${temptablename} xls ${destserver} ${destuser} ${destpasswd} ${tempfilename}
	if [ $? != 0 ]
	then
	    echo "${shname}: Error copying data!"
	    exit 1
	fi

	rm ${tempfilename}

	# now truncate the table and insert all rows from the temporary table
	$XLS/src/scripts/transfer/CompanyCopy/TruncateAndInsert.sh ${destserver} ${destuser} ${destpasswd} ${tbl} ${temptablename}
	if [ $? != 0 ]
	then
	    echo "${shname}: Error truncating and inserting data from temp table!"
	    exit 1
	fi

done

exit 0
