# copyaccount.sh - copies account data from one server to another
#parse the command line arguments
# 1 = source server
# 2 = source login
# 3 = source passwd
# 4 = dest server
# 5 = dest user
# 6 = dest passwd
# 7 = accountID
# 8 = temp file

shname=copyaccount.sh
if [ $# -lt 7 ]
then
    echo "Usage: ${shname} srcserver srclogin srcpasswd destserver destlogin destpasswd accountID tempfile"
    exit 1
fi


srcserver=${1}
srcuser=${2}
srcpasswd=${3}
destserver=${4}
destuser=${5}
destpasswd=${6}
a=${7}
if [ $# -gt 7 ]
then
  echo "You are copying all tables without confirmation."
  echo "Are you sure you want to continue?"
  read z;
  if [ "${z}" != "y" -a "${z}" != "Y" ]; then echo "Aborting ..."; exit 1; fi;
  confirm=1
  tempfile=${8}
else
  confirm=0
  tempfile=${a}.txt
fi

allacombos="account/id account_perm/account accounts_flag/accountId accountmenuitemflags/account_id pricing/account pricing_strings/account validation_list_projects/account validation_regexp_projects/Account pbaccountinfo/account"
for c in $allacombos
do
	table="${c%%/*}"
	key="${c##*/}"

	if [ $confirm -eq 1 ]; then z="y"; else echo "Copy ${table}?";read z; fi;
	if [ "${z}" = "y" ]
	then
		logfile="copyaccount_${table}.log"
		if [ -e ${logfile} ]
		then
			rm -f ${logfile}
		fi

		echo "Copying ${table}.."

		querybase="from ${table} where ${key} = ${a}"
		query="select * ${querybase}"
		deletequery="delete ${querybase}"
#echo "query=${query}"
		# delete any existing data
		isql /Q "${deletequery}" /S ${destserver} /U ${destuser} /P ${destpasswd} -b -n -h-1 -w4096 >> ${logfile} 2>&1
		if [ $? != 0 ]
		then
		    echo "${shname}: Error deleting existing data on ${destserver}!"
		    echo "${shname}: Error deleting existing data on ${destserver}!" >> ${logfile}
		    exit 1
		fi

		# copy over the data from one server to another
		$XLS/src/scripts/transfer/copyselectedrowslow.sh "${query}" xls ${srcserver} ${srcuser} ${srcpasswd} ${table} xls ${destserver} ${destuser} ${destpasswd} ${tempfile} >> ${logfile} 2>&1
		if [ $? != 0 ]
		then
		    echo "${shname}: Error copying data!"
		    echo "${shname}: Error copying data!" >> ${logfile}
		    exit 1
		fi

#		rm ${logfile}
	fi
done



allucombos="users/id users_flag/userid usermenuitemflags/user_id prepbookusers/userid prepbookusersetting/userid prepbooksection/userid"
for c in $allucombos
do
	table="${c%%/*}"
	key="${c##*/}"

	if [ $confirm -eq 1 ]; then z="y"; else echo "Copy ${table}?";read z; fi;
	if [ "${z}" = "y" ]
	then
		logfile="copyaccount_${table}.log"
		if [ -e ${logfile} ]
		then
			rm -f ${logfile}
		fi

		echo "Copying ${table}.."

		querybase="from ${table} where ${key} IN (select id from users where account = ${a})"
		query="select * ${querybase}"
		deletequery="delete ${querybase}"

		# delete any existing data
		isql /Q "${deletequery}" /S ${destserver} /U ${destuser} /P ${destpasswd} -b -n -h-1 -w4096 >> ${logfile} 2>&1
		if [ $? != 0 ]
		then
		    echo "${shname}: Error deleting existing data on ${destserver}!"
		    echo "${shname}: Error deleting existing data on ${destserver}!" >> ${logfile}
		    exit 1
		fi

		# copy over the data from one server to another
		$XLS/src/scripts/transfer/copyselectedrowslow.sh "${query}" xls ${srcserver} ${srcuser} ${srcpasswd} ${table} xls ${destserver} ${destuser} ${destpasswd} ${tempfile} >> ${logfile} 2>&1
		if [ $? != 0 ]
		then
		    echo "${shname}: Error copying data!"
		    echo "${shname}: Error copying data!" >> ${logfile}
		    exit 1
		fi

#		rm ${logfile}
	fi
done


exit 0
