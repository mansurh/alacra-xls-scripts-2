#parse the command line arguments
# 1 = source query
# 2 = sourcedb
# 3 = source server
# 4 = source login
# 5 = source passwd
# 6 = dest table
# 7 = dest db
# 8 = dest server
# 9 = dest user
# 10 = dest passwd
# 11 = temp file

shname=copyselectedrowslow.sh
if [ $# -lt 11 ]
then
    echo "Usage: ${shname} srcquery srcdb srcserver srclogin srcpasswd desttable destdb destserver destlogin destpasswd tempfile [truncflag]"
    exit 1
fi


query=${1}
srcdb=${2}
srcserver=${3}
srcuser=${4}
srcpasswd=${5}
desttable=${6}
destdb=${7}
destserver=${8}
destuser=${9}
destpasswd=${10}
tempfile=${11}
if [ $# -lt 12 ]
then
    truncflag=0
else
	truncflag=${12}
fi


# delete temp file
rm -f ${tempfile}


# query the data out
bcp "${query}" queryout ${tempfile} /S${srcserver} /U${srcuser} /P${srcpasswd} /c /t"\001" /r"\002"
if [ $? != 0 ]
then
    echo "${shname}: Error saving source data to file"
    exit 1
fi


# truncate, if asked
if [ "${truncflag}" = "1" ]
then
	isql /n /U${destuser} /P${destpasswd} /S${destserver} /Q"use ${destdb} truncate table ${desttable}"
	if [ $? != 0 ]
	then
	    echo "Error truncating  ${desttable} in ${destdb}"
	    exit 1
	fi
fi

# load the data in
bcp ${destdb}.dbo.${desttable} in ${tempfile} /E /b1000 /S${destserver} /U${destuser} /P${destpasswd} /c /t"\001" /r"\002"
if [ $? != 0 ]
then
    echo "${shname}: Error loading destination data from file"
    exit 1
fi

exit 0
