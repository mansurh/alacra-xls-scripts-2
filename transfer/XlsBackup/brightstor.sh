if [ $# -lt 1 ]
then
	echo "usage: $0 stop|start [server] "
	exit 1
fi

action=$1
if [ ${action} != "stop" -a ${action} != "start" ]
then
  echo "usage: action can be stop or start only"
  exit 1
fi

server=${COMPUTERNAME}
if [ $# -gt 1 ]
then
  server=$2
fi

BRIGHTSTOR_DIR="C:/Program Files/CA/BrightStor High Availability"
cd "${BRIGHTSTOR_DIR}"
echo `pwd`
echo "bhatask.exe -${action}:${server}"

bhatask.exe -${action}:${server}

