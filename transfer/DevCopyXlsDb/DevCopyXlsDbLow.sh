# DevCopyXlsDbLow.sh
# Parse the command line arguments.
#	1 = source server
#	2 = source user
#	3 = source psw
#	4 = dest server
#	5 = dest user
#	6 = dest psw
#	7 = noget (optional)

shname=DevCopyXlsDbLow.sh
if [ $# -lt 6 ]
then
	echo "Usage: ${shname} sourceserver sourceuser sourcepsw destserver destuser destpsw"
	exit 1
fi

# Save the runstring parameters in local variables
sourceserver=$1
sourceuser=$2
sourcepsw=$3
destserver=$4
destuser=$5
destpsw=$6

# optional 'noget' as 7th param skips source safe step
if [ $# -gt 6 ]
then
	noget=$7
else
	noget=N
fi


# get the latest exclusions
# create paths with slashes both ways
ourpath="$XLSDATA/DevCopyXlsDb"
backslashpath=$(sed -e"s/\//\\\\/g" << HERE
${ourpath}
HERE
)

#echo "ourpath=${ourpath}"
#echo "backslashpath=${backslashpath}"

# source safe options
ssproject="\$/xls/src/scripts/transfer/DevCopyXlsDb"
# set the 'ignore prompts' flag (useful to remove for debugging)
iflag=-I-N
#iflag=
sslogin=batchuser
sspsw=batchuser

# Get the latest procedure exclusions
procexclfile=${ourpath}/excl_procedures.txt
if [ "${noget}" != "noget" ]
then
	echo "${shname}: Getting latest ${procexclfile}"
	rm -f ${procexclfile}
	ss Get ${ssproject}/excl_procedures.txt -GL${backslashpath} -GWR -GTM -GCC -R ${iflag} -Y${sslogon},${sspsw}
	if [ $? -ne 0 ]
	then
		echo "${shname}: Source Safe Get failed (1)"
		exit 1
	fi
	if [ ! -e ${procexclfile} ]
	then
		echo "${shname}: ${procexclfile} does not exist after Source Safe Get"
		exit 1
	fi
	chmod 777 ${procexclfile}
	echo ""
fi

# Get the latest table exclusions
tableexclfile=${ourpath}/excl_tables.txt
if [ "${noget}" != "noget" ]
then
	echo "${shname}: Getting latest ${tableexclfile}"
	rm -f ${tableexclfile}
	ss Get ${ssproject}/excl_tables.txt -GL${backslashpath} -GWR -GTM -GCC -R ${iflag} -Y${sslogon},${sspsw}
	if [ $? -ne 0 ]
	then
		echo "${shname}: Source Safe Get failed (2)"
		exit 1
	fi
	if [ ! -e ${tableexclfile} ]
	then
		echo "${shname}: ${tableexclfile} does not exist after Source Safe Get"
		exit 1
	fi
	chmod 777 ${tableexclfile}
	rm -f ${ourpath}/vssver.scc
	echo ""
fi

# Perform the selective copy, using the exclusion files
$XLS/src/scripts/transfer/copydatabase.sh xls ${sourceserver} ${sourceuser} ${sourcepsw} xls ${destserver} ${destuser} ${destpsw} ${ourpath} ${tableexclfile} ${procexclfile}
returncode=$?

exit $returncode
