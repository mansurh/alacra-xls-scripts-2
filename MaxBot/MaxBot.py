#!/usr/bin/python3 

import os
import subprocess as sproc
import sys
import re


# See if this file is latest
cmd = r'ss diff $/xls/src/scripts/MaxBot/MaxBot.py "c:\Users\xls\src\scripts\MaxBot\MaxBot.py"'
try:
    diff = sproc.check_output(cmd, shell=True)
except sproc.CalledProcessError:
    diff = b'notsame'
if not len(re.findall('No differences.', diff.decode())) > 0:
    print('MaxBot.py is out of date, please get latest recursively from source safe ($/xls/src/scripts/MaxBot)')
    sys.exit()

# Get latest version of all the stuff
cmd = r'ss diff $/xls/src/scripts/MaxBot/lib/Version.txt "c:\Users\xls\src\scripts\MaxBot\lib\Version.txt"'
try:
    diff = sproc.check_output(cmd, shell=True)
except sproc.CalledProcessError:
    diff = b'notsame'
if not len(re.findall('No differences.', diff.decode())) > 0:
    print('Version difference detected, getting latest MaxBot code from sourcesafe')
    sproc.check_output(r"ss get -GWR -GL'c:\Users\xls\src\scripts\MaxBot' $/xls/src/scripts/MaxBot/_MaxBot.py -I-N", shell=True)
    sproc.check_output(r"ss get -GWR -R -GL'c:\Users\xls\src\scripts\Maxbot\lib' $/xls/src/scripts/MaxBot/lib -I-N", shell=True)

# activate virtualenv
virtualenvhome = os.getenv('WORKON_HOME')
mbv = os.path.join(virtualenvhome, 'maxbot/bin/activate_this.py')
mbv_old = os.path.join(virtualenvhome, 'run_sql/bin/activate_this.py')

if not os.path.exists(mbv) and os.path.exists(mbv_old):
    mbv = mbv_old
if os.path.exists(mbv):
    exec(compile(open(mbv, 'rb').read(), mbv, 'exec'), dict(__file__=mbv))
else:
    print('Did not find maxbot virtualenv')

# run Maxbot
cmb = ['python3', os.path.join(os.path.dirname(os.path.realpath(__file__)), '_MaxBot.py')]
cmb.extend(sys.argv[1:])
sproc.call(cmb)
