XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

IPIDdatabase=concordance

IPIDserver=`get_xls_registry_value ${IPIDdatabase} Server`
IPIDlogin=`get_xls_registry_value ${IPIDdatabase} User`
IPIDpassword=`get_xls_registry_value ${IPIDdatabase} Password`

cd $XLSDATA/matching

output_file=results.txt
bcp "exec get_ipid_stale_list" queryout ${output_file} /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /c 
if [ $? -ne 0 ]
then
	blat - -body "error in getting info from dmo_dbavailable on ${IPIDserver}" -s "Failed to get ipid schedule" -t "ipidalerts@alacra.com"
	exit 1
fi

output_file2=results2.txt
bcp "select name, crdate From sysobjects where name like 'ipid%site_agent_new' order by name" queryout ${output_file2} /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /c 
if [ $? -ne 0 ]
then
	blat - -body "error in getting info from dmo_dbavailable on ${IPIDserver}" -s "Failed to get ipid schedule" -t "ipidalerts@alacra.com"
	exit 1
fi

if [ -s ${output_file2} ]
then
  echo >> ${output_file}
  echo "Unrenamed site_agent tables" >> ${output_file}
  cat ${output_file2} >> ${output_file}
fi

if [ -s ${output_file} ]
then
	blat ${output_file} -t "ipidalerts@alacra.com" -s "List of stale ipid tables on ${IPIDserver}"
fi
