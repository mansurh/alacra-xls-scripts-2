# Script file to generate the IP matching reports for argus
# 1 = Output filename
# 2 = XLS Server
# 3 = XLS Login
# 4 = XLS Password
if [ $# -lt 4 ]
then
	echo "Usage: ipid_argus_report.sh reportfile xls_server xls_login xls_password"
	exit 1
fi

REPORTFILE=$1
xlsserver=$2
xlslogin=$3
xlspassword=$4

ipname=argus

# Name of the table to use
TABLENAME=ipid_${ipname}

isql -S${xlsserver} -U${xlslogin} -P${xlspassword} -s"|" -n -w1000 -h0 >${REPORTFILE} << HERE
SET NOCOUNT ON

/* Required Report - New companies to appear in database */
PRINT "Potentially new companies in argus"
select i.id, i.name from ipid_argus i
where i.id not in (
  select sourcekey from security_map where source = 3
)

/* Required Report - Companies which have disappeared from database */
PRINT "Matched companies that have disappeared from argus"
/* select id, sourcekey from security_map sm, security s */
/* where source = 2 */
/* and sourcekey not in */
 select s.issuer, s.name, sm.id, s.cusipcins, s.ticker, s.exchange
 from security_map sm, security s
 where sm.source = 3
 and sm.id = s.id
 and sm.sourcekey not in 
(
  select id from ipid_argus
)

/* Optional report - new information available in database */

/* Optional report - companies with name changes.  This is only */
/* valid for Finex where naming methodologies are similar. */

HERE
