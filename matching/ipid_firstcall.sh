XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

#Script file to build the IP matching table for firstcall
# Created 6 January 2004 by John Willcox
WD=$XLS/src/scripts/matching
DATADIR=$XLSDATA/matching/firstcall
mkdir -p ${DATADIR}
cd ${DATADIR}

# 1 = IPID Server
# 2 = IPID Database
# 3 = IPID Login
# 4 = IPID Password

ARGS=7
if [ $# -ne $ARGS ]
then
	echo "Usage: ipid_firstcall.sh ipid_server ipid_database ipid_login ipid_password " 
	exit 1
fi

IPIDserver=$1
IPIDdatabase=$2
IPIDlogin=$3
IPIDpassword=$4
#FTPserver=$5
#FTPlogin=$6
#FTPpassword=$7
FTPserver=`get_xls_registry_value firstcall Server 6`
FTPlogin=`get_xls_registry_value firstcall User 6`
FTPpassword=`get_xls_registry_value firstcall Password 6`

ftpfilename="tfn_id"

getFilename="${ftpfilename}.zip"
textFilename="${ftpfilename}.txt"
editFilename="${ftpfilename}ED.txt"

ftpFilename="command.ftp"

formatFile="${WD}/ipid_firstcallbcp.fmt"
countrySQLFile="${WD}/ipid_firstcallCountryMap.sql"

IPIDtable="ipid_firstcall"


# Build the ftp command file to download the data file

rm -f ${ftpFilename}
echo "user ${FTPlogin} ${FTPpassword}" > ${ftpFilename}
echo "binary" >> ${ftpFilename}
echo "get $getFilename" >> ${ftpFilename}
echo "quit" >> ${ftpFilename}

# Ftp the data file

echo "Retrieving data file from IBES FTP server" 
ftp -i -n -s:${ftpFilename} ${FTPserver} 
if [ $? != 0 ]
then
	echo "FTP process failed - exiting" 
	exit 1
fi

# Make sure output file exists

if [ ! -e $getFilename ]
then
	echo "Data file not found - exiting" 
	exit 1
fi

# Unzip the file

echo "Unzipping the file" 
unzip -o $getFilename
if [ $? != 0 ]
then
	echo "Unzip process failed - exiting" 
	exit 1
fi

# Formatting file

echo "Formatting file" 
sed -e 1d $textFilename > $editFilename
if [ $? != 0 ]
then
	echo "Could not format file - exiting" 
	exit 1
fi

# Drop the old ipid table

echo "Connecting to SQL server and dropping ${IPIDtable}" 
isql -n -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} << ENDSQL 
drop table ${IPIDtable}
go
ENDSQL
if [ $? != 0 ]
then
	echo "Connecting to SQL server failed - exiting" 
	exit 1
fi

# Create the new ipid table

echo "Creating new IPID table ${IPIDtable}" 
isql -n -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} << ENDSQL 
create table ${IPIDtable} (
	MatchKey varchar(50) not NULL,
	Name varchar(100) NULL,
	Cusip varchar(50) NULL,
	Sedol varchar(50) NULL,
	ISIN varchar(50) NULL,
	RIC varchar(50) NULL,
	Ticker varchar(50) NULL,
	Country varchar(50) NULL,
	RunDate varchar(50) NULL
)
GO
ENDSQL
if [ $? != 0 ]
then
	echo "Could not create table - exiting" 
	exit 1
fi

# Bulk copy programming ${editFilename} to ${IPIDtable}

echo "Bulk copy programming ${editFilename} to ${IPIDtable}" 
bcp ${IPIDtable} in ${editFilename} /U${IPIDlogin} /P${IPIDpassword} /S${IPIDserver} /f${formatFile} /t /b 5000 
if [ $? != 0 ]
then
	echo "BCP not completed - exiting" 
	exit 1
fi

# Formatting file with Alacra countries

echo "Formatting file with Alacra countries" 
isql -n -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} -i ${countrySQLFile} 
if [ $? != 0 ]
then
	echo "Problem formatting file - exiting" 
	exit 1
fi

# Create indices

echo "Creating indices" 
isql -n -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} << ENDSQL 
create index ${IPIDtable}_01 on ${IPIDtable}
	(MatchKey)
GO
create index ${IPIDtable}_02 on ${IPIDtable}
	(Name)
GO
create index ${IPIDtable}_03 on ${IPIDtable}
	(Cusip)
GO
create index ${IPIDtable}_04 on ${IPIDtable}
	(Sedol)
GO
create index ${IPIDtable}_05 on ${IPIDtable}
	(ISIN)
GO
create index ${IPIDtable}_06 on ${IPIDtable}
	(RIC)
GO
create index ${IPIDtable}_07 on ${IPIDtable}
	(Ticker)
GO
create index ${IPIDtable}_08 on ${IPIDtable}
	(Country)
GO
ENDSQL
if [ $? != 0 ]
then
	echo "Indices not created - exiting" 
	exit 1
fi

# Clean up

echo "Cleaning up" 
rm ${getFilename}
rm ${textFilename}
rm ${editFilename}
rm ${ftpFilename}
cd ..
if [ $? != 0 ]
then
	echo "Problem cleaning up - exiting" 
	exit 1
fi

echo "Done" 
