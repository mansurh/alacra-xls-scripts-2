XLSUTILS=$XLS/src/scripts/loading/dba/
. $XLSUTILS/get_registry_value.fn
. $XLSUTILS/compare_bcpRowsCopied_to_tableSize.fn
. $XLSUTILS/check_return_code.fn

#Convert views to tables

if [ $# -eq 0 ]; then
    printf "RUN example: viewToTable.sh dbname step
dbname: Database we are extracting data from 
step: numerical value of what part/step of the process to start from
If this is the first time you're running, please create the table 
manually in the concordance database, create 2 files both called 
create_index.sql and create_table.sql for both regular table and 
kwic table in ../matching/ViewToTables/(ClientName) 
and ../matching/ViewToTables/(ClientName)/kwic."
	
	printf "Main Steps
STEP 1: Check Existance of Tables
STEP 2: Load into file all data needed for insertion 
STEP 3: Drop _new table 
STEP 4: Create _new table 
STEP 5: Drop _bak table
STEP 6: Insert all data from file created in step 2
STEP 7: Create Indices for the table

Step Logic: 
1-7: Main process for regular table for specified client
8-14: Main process for Kwic Table
15: Rename Kwic Table
16: Rename Regular table
ONLY FOR DNBUM: 
22-28: Main process for dnbUM processes
29-35: Main process for dnbUM kwic
36: Rename dnbUM kwic
37: Rename dnbUm"
	
	exit 1
fi

dbname=$1 

step=1
if [ $# -gt 1 ]
then
  step=$2
fi
if [ ${step} -eq 0 ]
then
	step=1
fi

SCRIPTDIR="$XLS/src/scripts/matching/viewToTable"
cbase=concordance

date

server=`get_xls_registry_value "${dbname}" "Server" "0"`
duser=`get_xls_registry_value "${dbname}" "User" "0"`
dpass=`get_xls_registry_value "${dbname}" "Password" "0"`

cserver=`get_xls_registry_value "${cbase}" "Server" "0"`
cuser=`get_xls_registry_value "${cbase}" "User" "0"`
cpass=`get_xls_registry_value "${cbase}" "Password" "0"`



timestamp() {
  date +"%T"
}


checkTableExist()
{
	echo "Checking Table Existance"
	exist="SET NOCOUNT ON; IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '${tableName}') begin Select '0' END"
	
	result="$(osql /b /S${cserver} /U${cuser} /P${cpass} /Q "${exist}" -h-1)"

	if [[ ! -z "${result}" ]]
	then
		echo "${tableName} Does not exist. Please create it and then run script again, exiting ..."
		exit 1
	fi
		
}

#each database contains its own storedproc dump_ipid_data referring to the ipid table we already need to use. We must specify if it's a kwic table 
bcpTable() {
	echo "Starting BCP from tables that create the view ${tableName}"
	echo "bcp exec dump_ipid_data${kwicFile} ${extraParamUM} queryout ${tableName}.dat -S${server} -U${duser} -Ppassword -c  -t|  -b 1000 -e ${tableName}_errorOut.err -o ${tableName}_out.txt"
	bcp "exec dump_ipid_data${kwicFile} ${extraParamUM}" queryout ${tableName}.dat -S${server} -U${duser} -P${dpass}  -N  -b 1000 -e ${tableName}_errorOut.err -o ${tableName}_out.txt
	
	check_return_code $? "Error with BCP ... Aborting" 1

}

drop(){
	echo "drop table"
	drop="IF  EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '${tableName}_new' and TABLE_TYPE = 'BASE TABLE')
			begin
				DROP TABLE ${tableName}_new
			end; "		

	osql /S${cserver} /U${cuser} /P${cpass} /b /Q "${drop}"

	check_return_code $? "Error with dropping ${tableName}_new table ... Aborting" 1
	
}	

createNewTable(){
	echo "create new table"
	
	createNew="select top 0 * into ${tableName}_new from ${tableName}; "
	 
	osql /S${cserver} /U${cuser} /P${cpass} /b /Q "$createNew"

	check_return_code $? "Error creating ${tableName}_new table ... Aborting" 1
}

dropBak(){
	echo "Dropping Backup File for $tableName and $tableName_kwic"
	dropBack="IF  EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '${tableName}_bak' and TABLE_TYPE = 'BASE TABLE') begin drop table ${tableName}_bak END;"
	osql /b /S${cserver} /U${cuser} /P${cpass} /b /Q "${dropBack}" 
		
	check_return_code $? "Error with dropping ${tableName}_bak table ... Aborting" 1
}
	
populate(){
	echo "populating new table ${tableName}_new " 
	echo "bcp ${tableName}_new in ${tableName}.dat -S${cserver} -U${cuser} -P${cpass} -c -t| -e ${tableName}_errorin.err -o ${tableName}_in.txt -b 5000"
	bcp ${tableName}_new in ${tableName}.dat -S${cserver} -U${cuser} -P${cpass} -N -e ${tableName}_errorin.err -o ${tableName}_in.txt -b 5000

	check_return_code $? "Error populating ${tableName}_new table ... Aborting" 1
	
    compare_bcpRowsCopied_to_tableSize ${cserver} ${cbase} ${tableName}_new ${tableName}_out.txt 0 1
}	
	
createIndices(){
	
	if [ -z ${kwicFile} ]
	then	
		directory="${SCRIPTDIR}/${indexFolder}/create_index.sql"
	else
		directory="${SCRIPTDIR}/${indexFolder}/kwic/create_index.sql"
	fi
	
	echo "Creating Indices"
	osql /S${cserver} /U${cuser} /P${cpass} /b /i ${directory}
		
	check_return_code $? "Error creating indices for ${tableName}_new table ... Aborting" 1
}

rename(){
	echo "Rename original file to back up and new file to orignal"
	rename="exec sp_rename '${tableName}', '${tableName}_bak';
	exec sp_rename '${tableName}_new', '${tableName}'"
	
	osql /S${cserver} /U${cuser} /P${cpass} /b /Q "${rename}" 
	check_return_code $? "Error renaming tables ... Aborting" 1
}

mainProcess(){	
	mainStep=$(($step%7))
	if [ ${mainStep} -eq 0 ]
	then
		mainStep=7
	fi
	
	echo "Starting process for $tableName"
	#STEP 1: Check Existance of Tables
	if [ $mainStep -eq 1 ]
	then
	  echo "Step $mainStep for $tableName"
	  checkTableExist
	  timestamp
	  if [ $? != 0 ]; then return 1; fi
	  step=$(($step+1))
	  mainStep=$((mainStep+1))
    fi	
	#STEP 2: Load into file all data needed for insertion 
	if [ $mainStep -eq 2 ]
	then
	  echo "Step $mainStep for $tableName"
	  bcpTable
	  timestamp	  
	  if [ $? != 0 ]; then return 1; fi
	  step=$(($step+1))
	  mainStep=$((mainStep+1))
    fi
	#STEP 3: Drop _new table 
	if [ $mainStep -eq 3 ]
	then
	  echo "Step $mainStep for $tableName"
	  drop
	  if [ $? != 0 ]; then return 1; fi
	  step=$(($step+1))
	  mainStep=$((mainStep+1))
    fi
	#STEP 4: Create _new table 
	if [ $mainStep -eq 4 ]
	then
	  echo "Step $mainStep for $tableName"
	  createNewTable
	  if [ $? != 0 ]; then return 1; fi
	  step=$(($step+1))
	  mainStep=$((mainStep+1))
    fi
	#STEP 5: Drop _bak table
	if [ $mainStep -eq 5 ]
	then
	  echo "Step $mainStep for $tableName"
	  dropBak
	  timestamp
	  if [ $? != 0 ]; then return 1; fi
	  step=$(($step+1))
	  mainStep=$((mainStep+1))
    fi
	#STEP 6: Insert all data from file created in step 2
	if [ $mainStep -eq 6 ]
	then
	  echo "Step $mainStep for $tableName"
	  populate
	  timestamp
	  if [ $? != 0 ]; then return 1; fi
	  step=$(($step+1))
	  mainStep=$((mainStep+1))
    fi	
	#STEP 7: Create Indices for the table
	if [ $mainStep -eq 7 ]
	then
	  echo "Step $mainStep for $tableName"
	  createIndices
	  if [ $? != 0 ]; then return 1; fi
	  step=$(($step+1))
	  mainStep=$((mainStep+1))
    fi	
	
	echo "Completed process for $tableName"
}


main()
{
if [ ${dbname} == "dnb2" ]
then
		tableName="ipid_dnb"		
else
		tableName="ipid_${dbname}"
fi

#$kwicFile lets us know we are up to kwic table and used mainly in bcpTable function to specify what stored proc to use in bcp
kwicFile=""

#$table1 is used mainly for rename process after both mainProcess functions have completed, so we don't forget what the first table was called
table1=${tableName}

#$indexFolder is used in createIndex function to help find directory of create_index.sql
indexFolder=`echo "${tableName}" | cut -d'_' -f 2`\

if [ ${step} -le 7 ]
then
	mainProcess
fi

#process for kwic tables
tableName="${tableName}_kwic"
kwicFile="_kwic"

if [ ${step} -le 14 ]
then
	mainProcess
fi

#Both mainProcess's ending. Now we rename kwic table first as tableName variable already has table kwic name
if [ ${step} -eq 15 ]
then
	rename 
	step=$(($step+1))
fi

if [ ${step} -eq 16 ]
then
	tableName=${table1}
	rename
fi

echo "********$step********"
#process for UM table in dnb2 database. Same process as above, excpt dnb has an extra table called ipid_dnbUM.
if [ ${dbname} == "dnb2" ]
then	
	if [ ${step} -lt 22 ]
	then 
		step=22
	fi

	tableName="ipid_dnbUM"
	kwicFile=""
	extraParamUM="UM"
	table1=${tableName}
	indexFolder=`echo "${tableName}" | cut -d'_' -f 2`
	if [ ${step} -le 28 ]
	then
		mainProcess
	fi	
	
	tableName="${tableName}_kwic"
	kwicFile="_kwic"
	if [ ${step} -le 35 ]
	then
		mainProcess
	fi	
	
	#renames kwic table first as tableName variable already has table kwic name
	if [ ${step} -eq 36 ]
	then
		rename 
		step=$(($step+1))
	fi

	if [ ${step} -eq 37 ]
	then
		tableName=${table1}
		rename
	fi

fi

echo "DONE"
}

# make sure our load directory exist
mkdir -p ${loaddir}/log
mkdir -p $XLSDATA/viewToTable/log
logfile=$XLSDATA/viewToTable/log/`date +%Y-%m-%d`__${dbname}_ViewToTable.log

(main) > ${logfile} 2>&1 
if [ $? -ne 0 ]
then
	blat ${logfile} -s "Script failed on Step: $step" -t administrators@alacra.com
	exit 1
fi
#-------------------------------------------------------

exit 0



