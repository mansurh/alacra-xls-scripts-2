# Script file to build the IP matching table for gsi
# 1 = IPID Server
# 2 = IPID Login
# 3 = IPID Password
if [ $# -lt 7 ]
then
	echo "Usage: ipid_gsi.sh ipid_server ipid_database ipid_login ipid_password gsiserver gsilogin gsipassword"
	exit 1
fi

ipidserver=$1
ipiddatabase=$2
ipidlogin=$3
ipidpassword=$4
gsiserver=$5
gsilogin=$6
gsipassword=$7

ipname=gsi

mkdir -p $XLSDATA/matching/${ipname}
cd $XLSDATA/matching/${ipname}

# Name of the temp file to use
TMPFILE1=ipid_company.pip
TMPFILE2=ipid_company2.pip

# Name of the table to use
TABLENAME=ipid_${ipname}

# Step 1 - remove any old temp files
rm -f ${TMPFILE1} 

#Step 2 - get new data into bcp file
isql -S${gsiserver} -U${gsilogin} -P${gsipassword} -s"|" -w1000 -n  -h-1 >${TMPFILE1} << HERE
SET NOCOUNT ON
select t.name, t.cik, t.ticker, t.exchange from gsi..ticker t where active='A'
HERE

# Step 3 - post-process the temp file
sed -e "s/,/\^/g;s/|/,/g" < ${TMPFILE1} > temp1.tmp 
qcd2pipe -l -r < temp1.tmp > temp2.tmp
sed -e "s/\^/,/g;s/NULL//g" < temp2.tmp > ${TMPFILE2}

# Step 2 - drop the old id table - don't check for error
# as it may not exist
isql /S${ipidserver} /U${ipidlogin} /P${ipidpassword} << HERE
drop table ${TABLENAME}_prev
exec sp_rename '${TABLENAME}', '${TABLENAME}_prev'
go
HERE

# Step 3 - create the new table
isql /S${ipidserver} /U${ipidlogin} /P${ipidpassword} << HERE
create table ${TABLENAME} (
	name varchar(120) NOT NULL,
	cik char(10) NULL,
	ticker char(12) NULL,
	exchange char(6) NULL

)
GO
create index ${TABLENAME}_01 on ${TABLENAME}
	(cik)
GO
create index ${TABLENAME}_02 on ${TABLENAME}
	(ticker)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi


bcp ${TABLENAME} in ${TMPFILE2} /S${ipidserver} /U${ipidlogin} /P${ipidpassword} /b1000 /c /t"|"
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi


# clean up
#rm -f ${TMPFILE1} temp1.tmp temp2.tmp
