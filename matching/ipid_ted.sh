# Script file to build the IP matching table for Investext
# 1 = XLS Server
# 2 = XLS Login
# 3 = XLS Password
# 4 = ted Server
# 5 = ted Login
# 6 = ted Password
if [ $# -lt 6 ]
then
	echo "Usage: ipid_ted.sh xls_server xls_login xls_password ted_server ted_login ted_password"
	exit 1
fi

xlsserver=$1
xlslogin=$2
xlspassword=$3
tedserver=$4
tedlogin=$5
tedpassword=$6

ipname=ted

# Name of the temp file to use
TMPFILE1=ipid_${ipname}1.tmp
TMPFILE2=ipid_${ipname}2.tmp

# Name of the table to use
TABLENAME=ipid_${ipname}

# Name of the format file to use
FORMAT_FILE=$XLS/src/scripts/matching/ipid_${ipname}.fmt

# Step 1 - remove any old temp files
rm -f ${TMPFILE1} ${TMPFILE2}

# Step 2 - select the ted data into a temporary file
isql -S${tedserver} -U${tedlogin} -P${tedpassword} -s"|" -n -w500 -h-1 >${TMPFILE1} << HERE
SET NOCOUNT ON
select 	
	distinct c.company_id, c.full_name, r.name, c.ticker, c.RIC, c.SEDOL, c.Extel, c.ISIN, c.VALOR
from
	ted..cmp c, ted..cmpDates d, ted..ctr r
where
	d.lastDataDate = (select max(lastDataDate) from cmpDates) 
	and 
	d.company_id=c.company_id
	and
	c.country_code = r.country_code
	and
	r.dataDate = (select max(lastDataDate) from cmpDates) 
	and 
	c.dataDate = (select max(lastDataDate) from cmpDates) 
HERE

# Step 3 - post-process the temp file
sed -f match.sed < ${TMPFILE1} > ${TMPFILE2}

# Step 4 - drop the old id table - don't check for error
# as it may not exist
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
drop table ${TABLENAME}
go
HERE

# Step 5 - create the new table
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
create table ${TABLENAME} (
	companyid varchar(15) NULL,
	name varchar(90) NULL,
	country varchar(40) NULL,
	ticker varchar(15) NULL,
	RIC varchar(15) NULL,
	SEDOL varchar(15) NULL,
	ExtelNo varchar(15) NULL,
	ISIN varchar(15) NULL,
	VALOR varchar(15) NULL
)
GO
create index ${TABLENAME}_01 on ${TABLENAME}
	(companyid)
GO
create index ${TABLENAME}_02 on ${TABLENAME}
	(name)
GO
create index ${TABLENAME}_03 on ${TABLENAME}
	(country)
GO
create index ${TABLENAME}_04 on ${TABLENAME}
	(ticker)
GO
create index ${TABLENAME}_05 on ${TABLENAME}
	(RIC)
GO
create index ${TABLENAME}_06 on ${TABLENAME}
	(SEDOL)
GO
create index ${TABLENAME}_07 on ${TABLENAME}
	(ExtelNo)
GO
create index ${TABLENAME}_08 on ${TABLENAME}
	(ISIN)
GO
create index ${TABLENAME}_09 on ${TABLENAME}
	(VALOR)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 6 - bcp in the select results
bcp ${TABLENAME} in ${TMPFILE2} /S${xlsserver} /U${xlslogin} /P${xlspassword} /f${FORMAT_FILE} /b100
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi

# Step 7 - map the country codes
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} < $XLS/src/scripts/matching/ipid_${ipname}_country.sql

# Step 8 - clean up
rm -f ${TMPFILE1} ${TMPFILE2}
