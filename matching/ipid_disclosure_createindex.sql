/****************************************************************************/
/* Create ipid_disclosure Indexes                                           */
/****************************************************************************/
USE xls
GO

IF EXISTS (SELECT * FROM sysindexes WHERE name = 'ipid_discl01')
BEGIN
   PRINT 'Dropping old version of index ipid_discl01'
   DROP index ipid_disclosure.ipid_discl01
END
GO

IF EXISTS (SELECT * FROM sysindexes WHERE name = 'ipid_discl02')
BEGIN
   PRINT 'Dropping old version of index ipid_discl02'
   DROP index ipid_disclosure.ipid_discl02
END
GO

IF EXISTS (SELECT * FROM sysindexes WHERE name = 'ipid_discl03')
BEGIN
   PRINT 'Dropping old version of index ipid_discl03'
   DROP index ipid_disclosure.ipid_discl03
END
GO

IF EXISTS (SELECT * FROM sysindexes WHERE name = 'ipid_discl04')
BEGIN
   PRINT 'Dropping old version of index ipid_discl04'
   DROP index ipid_disclosure.ipid_discl04
END
GO

/* */
PRINT ''
PRINT 'Creating Indexes:  ipid_discl01, ipid_discl02, ipid_discl03, ipid_discl04'
PRINT ''
/* */
GO

CREATE INDEX ipid_discl01 ON ipid_disclosure ( matchkey )
GO
CREATE INDEX ipid_discl02 ON ipid_disclosure ( name )
GO
CREATE INDEX ipid_discl03 ON ipid_disclosure ( cusip )
GO
CREATE INDEX ipid_discl04 ON ipid_disclosure ( ticker )
GO
