XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/check_arguments.fn
. $XLSUTILS/check_bcp_errors.fn
. $XLSUTILS/check_return_code.fn

check_arguments $# 7 "Usage: ipid_spcred.sh dest_server dest_database dest_login dest_password sp_server sp_login sp_password" 1

destdataserver=$1
destdb=$2
destlogon=$3
destpassword=$4
srcdataserver=$5
srclogon=$6
srcpassword=$7


LOADDIR=${XLS}/src/scripts/loading/spcred
TEMPDIR=${XLSDATA}/matching/spcred
mkdir -p ${TEMPDIR}

DATFILE="${TEMPDIR}/ipid_sp.dat"

#
# rebuild our own temporary ipid table in the spcred database, from which we
# build the ipid table in concordance
#

cmd="isql -U ${srclogon} -P ${srcpassword} -S ${srcdataserver} -r -i ${LOADDIR}/init_ipid_spcred_temp_from_ratings.sql"
echo "$cmd"
$cmd
check_return_code $? "init_ipid_spcred_temp_from_ratings.sql failed" 1

#
# Put new data in a batch file.
#

# bcp ${database}.dbo.${t} out ${t}.dat -U ${database} -P ${database} -S ${srcserver} -c

cmd="bcp spcred.dbo.ipid_spcred out ${DATFILE} /U${srclogon} /P${srcpassword} /S${srcdataserver} /f${LOADDIR}/ipid_spcred.fmt /e${TEMPDIR}/ipid_spcred.err -o ${TEMPDIR}/ipid_spcred.errout"
echo "$cmd"
echo ""
$cmd
check_return_code $? "Failed to bcp in spcred ipid table. Aborting" 2
check_bcp_errors ${TEMPDIR}/ipid_spcred.errout 5 "Failed to bcp out ipid_spcred table. Aborting" 1

#
# Drop old data from concordance table ipid_spcred
#

echo ""
echo "Dropping old ipid_spcred data on ${destdataserver}"
echo ""

echo "isql /U${destlogon} /P${destpassword} /S${destdataserver} < ${LOADDIR}/create_ipid_spcred.sql"
isql /U${destlogon} /P${destpassword} /S${destdataserver} < ${LOADDIR}/create_ipid_spcred.sql
check_return_code $? "sql query failed" 1

# drop the indices for the table
echo ""
echo "Dropping indices for ipid_spcred for faster loading"
echo ""

isql /U${destlogon} /P${destpassword} /S${destdataserver} < ${LOADDIR}/dropindex_ipid_spcred.sql


#
# Load all the new data...
#

# Load the ipid_spcred table
echo "Loading the ipid_spcred table" 

cmd="bcp ${destdb}.dbo.ipid_spcred in ${DATFILE} /U${destlogon} /P${destpassword} /S${destdataserver} /f${LOADDIR}/ipid_spcred.fmt /e${TEMPDIR}/ipid_spcred.err -o ${TEMPDIR}/ipid_spcred.errout"
echo "$cmd"
echo ""
$cmd
check_return_code $? "Failed to bcp in spcred ipid table. Aborting" 2
check_bcp_errors ${TEMPDIR}/ipid_spcred.errout 5 "Failed to bcp in ipid_spcred table. Aborting" 1

# Rebuild the indices for the table
echo ""
echo "Rebuilding indices for ipid_spcred for faster loading"
echo ""

isql /U${destlogon} /P${destpassword} /S${destdataserver} < ${LOADDIR}/createindex_ipid_spcred.sql

echo ""
echo "Updated table ipid_spcred for spcred OK."
echo ""

exit 0
