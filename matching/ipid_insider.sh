# Script file to build the IP matching table for Insider
# 1 = IPID Server
# 2 = IPID Database
# 2 = IPID Login
# 3 = IPID Password
# 4 = Insider Server
# 5 = Insider Login
# 6 = Insider Password
ARGS=7
if [ $# -ne $ARGS ]
then
	echo "Usage: ipid_Insider.sh ipid_server ipid_database ipid_login ipid_password Insider_server Insider_login Insider_password"
	exit 1
fi

IPIDserver=$1
IPIDdatabase=$2
IPIDlogin=$3
IPIDpassword=$4
ipserver=$5
iplogin=$6
ippassword=$7

ipname=Insider

# Name of the temp file to use
TMPFILE1=ipid_${ipname}1.tmp
TMPFILE2=ipid_${ipname}2.tmp

# Name of the table to use
TABLENAME=ipid_${ipname}

# Name of the format file to use
FORMAT_FILE=ipid_${ipname}.fmt

# Step 1 - remove any old temp files
rm -f ${TMPFILE1} ${TMPFILE2} ${TMPFILE3}

isql -S${ipserver} -U${iplogin} -P${ippassword} -s"|" -w500 -n -h-1 >${TMPFILE1} << TUN
SET NOCOUNT ON
select distinct securityid as matchkey, companyname, ticker, cusip from company where ticker is not null
TUN

# Step 3 - post-process the temp file
sed -f ${XLS}/src/scripts/matching/match.sed < ${TMPFILE1} > ${TMPFILE2}
# Step 4 - drop the old id table - don't check for error
# as it may not exist
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
drop table ${TABLENAME}
go
HERE

# Step 5 - create the new table
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
create table ${TABLENAME} (
	matchkey integer NULL,
	name varchar(60) NULL,
	ticker varchar(6) NULL,
	cusip varchar(9) NULL
)
GO
create index ${TABLENAME}_01 on ${TABLENAME}
	(matchkey)
GO
create index ${TABLENAME}_02 on ${TABLENAME}
	(ticker)
GO
create index ${TABLENAME}_04 on ${TABLENAME}
	(cusip)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 6 - bcp in the select results
bcp ${TABLENAME} in ${TMPFILE2} -S ${IPIDserver} -U ${IPIDlogin} -P ${IPIDpassword} -f ${XLS}/src/scripts/matching/${FORMAT_FILE} -b 100
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi

# step 8 - cross reference the country names
#isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} < ${XLS}/src/scripts/matching/ipid_${ipname}_country.sql

# Step 9 - clean up
#rm -f ${TMPFILE1} ${TMPFILE2} ${TMPFILE3}
