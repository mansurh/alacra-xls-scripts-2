XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# Arguments:
# 1 = ipid to match
# 2 = ipid database
# 3 = ipid server
# 4 = ipid password

if [ $# -lt 5 ]
then
	echo "Usage: dmo_events.sh ipidname IPID_Database IPID_Server IPID_Login IPID_Password [updateevents flag]"
	exit 1
fi

ipname=$1
IPIDdatabase=$2
IPIDserver=$3
IPIDlogin=$4
IPIDpassword=$5
updateevents=1
if [ "$6" = "0" -o "$6" = "n" -o "$6" = "N" ]
then
  updateevents=0
fi

DBA2database=dba2

DBA2server=`get_xls_registry_value ${DBA2database} Server`
DBA2login=`get_xls_registry_value ${DBA2database} User`
DBA2password=`get_xls_registry_value ${DBA2database} Password`

$XLS/src/scripts/matching/update_dbAvailable.sh ${IPIDserver} ${IPIDlogin} ${IPIDpassword} ${ipname}
if [ $? -ne 0 ]
then
   echo "Error in update_dbAvailable.sh, exiting ..."
   exit 1;
fi

if [ $updateevents -eq 0 ]
then
  echo "No need to update events, exiting ...";
  exit 0
fi

thisServer=$(echo $IPIDserver | tr [A-Z] [a-z]) #lower case version of the IPID server name

# if IPID server is production master server then generate ipid events
bcp "exec whereis ${IPIDdatabase}" queryout whereis.txt -c -S${DBA2server} -U${DBA2login} -P${DBA2password} -t "|"
if [ $? -ne 0 ]; then echo "Error in getting concordance list from dba2"; exit 1; fi;

IFS="|"

while read junk env server junk4 junk5 junk6 junk7 master junkend
do
  currServer=$(echo $server | tr [A-Z] [a-z]) #lover case version of the server name
  currEnv=$(echo $env | tr [A-Z] [a-z])  #lover case version of the environment type
  if [ ${currServer} = ${thisServer} ] && [ $currEnv = "production" ]
  then
	flag=$(echo $master|cut -c1| tr [A-Z] [a-z])
	echo "master=$flag"
	if [ "$flag" = "y" ]
	then
		echo "Running up_process_ipid_updates at: " `date`
		isql -b -S ${IPIDserver} -U ${IPIDlogin} -P ${IPIDpassword} -Q " exec up_process_ipid_updates '${ipname}'"
		if [ $? -ne 0 ]
		then
			echo "error generating ipid change events for ${ipname}"
			rm -f whereis.txt
			exit 1
		fi
		#break

		echo "Running up_process_matching_updates at: " `date`
		isql -b -S ${IPIDserver} -U ${IPIDlogin} -P ${IPIDpassword} -Q " exec up_process_matching_updates '${ipname}'"
		if [ $? -ne 0 ]
		then
			echo "error generating matching events for ${ipname}"
			rm -f whereis.txt
			exit 1
		fi
		#break
		
		echo "Running spUpdateSourceRegulations at: " `date`
		isql -b -S ${IPIDserver} -U ${IPIDlogin} -P ${IPIDpassword} -Q " exec spUpdateSourceRegulations '${ipname}'"
		if [ $? -ne 0 ]
		then
			echo "error updating regulation related data for ${ipname}"
			rm -f whereis.txt
			exit 1
		fi
		#break
		
	
	fi
  fi

done < whereis.txt

rm -f whereis.txt

#echo "Running spUpdateSnapshotFlags at: " `date`
#isql -b -S ${IPIDserver} -U ${IPIDlogin} -P ${IPIDpassword} -Q " exec spUpdateSnapshotFlags '${ipname}'"
#if [ $? -ne 0 ]
#then
#	echo "error updating snapshot flags for ${ipname}"
#	exit 1
#fi

exit 0

