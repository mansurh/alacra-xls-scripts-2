# 1 = cd Server
# 2 = cd Login
# 3 = cd Password
# 4 = bvd Server
# 5 = bvd Login
# 6 = bvd Password
if [ $# -lt 6 ]
then
	echo "Usage: ipid_bvd.sh cd_server cd_login cd_password bvd_server bvd_login bvd_password"
	exit 1
fi

cdserver=$1
cdlogin=$2
cdpassword=$3
ipserver=$4
iplogin=$5
ippassword=$6

year=`date +%Y`
month=`date +%m`
day=`date +%d`

updatedate="$year$month$day" 
scriptdir=$XLS/src/scripts/matching
logdir=${XLSDATA}/bvd

mkdir -p ${logdir}

# create unique log file name, based on date
logfilename=${logdir}/ipid_bvd_${updatedate}.log

echo "ipid_bvd.sh: Updating cd ipid table on ${cdserver}, updatedate=${updatedate}" > ${logfilename}

# Perform the update
${scriptdir}/update_ipid_bvd.sh ${cdserver} ${cdlogin} ${cdpassword} ${ipserver} ${iplogin} ${ippassword} >> ${logfilename} 2>&1
returncode=$?

# Mail the results to the administrators if update was no good
if [ ${returncode} != 0 ]
then
	echo "Bad return code from weekly update of ipid table for bvd - mailing log file" >> ${logfilename}
#	blat ${logfilename} -t "administrators@alacra.com,simon.vileshin@alacra.com" -s "Update of ipid table for bvd  failed"
	blat ${logfilename} -t "simon.vileshin@alacra.com" -s "Update of ipid table for bvd  failed"
else
	blat ${logfilename} -t "simon.vileshin@alacra.com" -s "Update of ipid table for bvd succeded"
#	rm -f ${logfilename}
fi

exit $returncode

