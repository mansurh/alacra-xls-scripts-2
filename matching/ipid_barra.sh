# Script file to build the IP matching table for barra
# 1 = IPID Server
# 2 = IPID Database
# 3 = IPID Login
# 4 = IPID Password
# 5 = barra Server
# 6 = barra Login
# 7 = barra Password
ARGS=7
if [ $# -ne $ARGS ]
then
	echo "Usage: ipid_barra.sh ipid_server ipid_database ipid_login ipid_password barra_server barra_login barra_password"
	exit 1
fi

IPIDserver=$1
IPIDdatabase=$2
IPIDlogin=$3
IPIDpassword=$4
barraserver=$5
barralogin=$6
barrapassword=$7

ipname=barra

SCRIPTDIR=$XLS/src/scripts/matching
DATADIR=$XLSDATA/matching/barra
mkdir -p ${DATADIR}
cd ${DATADIR}


# Name of the temp file to use
TMPFILE1=$ipid_${ipname}1.tmp
TMPFILE2=ipid_${ipname}2.tmp
TMPFILE3=ipid_${ipname}3.tmp

# Name of the table to use
TABLENAME=ipid_${ipname}
MKTCAP_TABLENAME=ipid_mktcap_${ipname}

# Name of the format file to use
FORMAT_FILE=$SCRIPTDIR/ipid_${ipname}.fmt

# Step 1 - remove any old temp files
rm -f ${TMPFILE1} ${TMPFILE2} ${TMPFILE3}

# Step 2 - select the barra data into a temporary file
isql /S${barraserver} /U${barralogin} /P${barrapassword} /s"|" /n /w500 /h-1 >${TMPFILE1} << HERE
SET NOCOUNT ON
select sedol 'matchkey', name, sedol 'cusip_sedol', NULL 'cap', 'Australia' 'country', asof
from aue_beta
where asof=(select max(asof) from aue_beta)
UNION 
select sedol 'matchkey', name, sedol 'cusip_sedol', NULL 'cap', 'Brazil' 'country', asof
from bre_beta
where asof=(select max(asof) from bre_beta)
UNION
select cusip8 'matchkey', name, cusip8 'cusip_sedol', NULL 'cap', 'Canada' 'country', asof
from cne_beta
where asof=(select max(asof) from cne_beta)
UNION
select sedol 'matchkey', name, sedol 'cusip_sedol', NULL 'cap', c.country_name 'country', asof
from eue_beta, country c
where asof=(select max(asof) from eue_beta) and c.country_id=country
UNION
select sedol 'matchkey', name, sedol 'cusip_sedol', NULL 'cap', 'France' 'country', asof
from fre_beta
where asof=(select max(asof) from fre_beta)
UNION
select sedol_cusip 'matchkey', name, sedol_cusip 'cusip_sedol', cap 'cap', c.country_name 'country', asof
from gemft_beta, country c
where asof=(select max(asof) from gemft_beta) and c.country_id=country
UNION
select sedol_cusip 'matchkey', name, sedol_cusip 'cusip_sedol', cap 'cap', c.country_name 'country', asof
from gemms_beta, country c
where asof=(select max(asof) from gemms_beta) and c.country_id=country
UNION
select sedol 'matchkey', name, sedol 'cusip_sedol', NULL 'cap', 'Germany' 'country', asof
from ger_beta
where asof=(select max(asof) from ger_beta)
UNION
select sedol 'matchkey', name, sedol 'cusip_sedol', NULL 'cap', 'Hong Kong' 'country', asof
from hke_beta
where asof=(select max(asof) from hke_beta)
UNION
select sedol 'matchkey', name, sedol 'cusip_sedol', NULL 'cap', 'Japan' 'country', asof
from jpe_beta 
where asof=(select max(asof) from jpe_beta)
UNION
select sedol 'matchkey', name, sedol 'cusip_sedol', NULL 'cap', 'Korea' 'country', asof
from kre_beta
where asof=(select max(asof) from kre_beta)
UNION
select sedol 'matchkey', name, sedol 'cusip_sedol', NULL 'cap', 'Netherlands' 'country', asof
from net_beta
where asof=(select max(asof) from net_beta)
UNION
select sedol 'matchkey', name, sedol 'cusip_sedol', NULL 'cap', 'South Africa' 'country', asof
from sae_beta 
where asof=(select max(asof) from sae_beta)
UNION
select sedol 'matchkey', name, sedol 'cusip_sedol', NULL 'cap', 'Singapore' 'country', asof
from sge_beta
where asof=(select max(asof) from sge_beta)
UNION
select sedol 'matchkey', name, sedol 'cusip_sedol', NULL 'cap', 'Switzerland' 'country', asof
from swi_beta
where asof=(select max(asof) from swi_beta)
UNION
select sedol 'matchkey', name, sedol 'cusip_sedol', NULL 'cap', 'Sweden' 'country', asof
from sne_beta
where asof=(select max(asof) from sne_beta)
UNION
select sedol 'matchkey', name, sedol 'cusip_sedol', NULL 'cap', 'Thailand' 'country', asof
from the_beta
where asof=(select max(asof) from the_beta)
UNION
select sedol 'matchkey', name, sedol 'cusip_sedol', NULL 'cap', 'Taiwan' 'country', asof
from twe_beta
where asof=(select max(asof) from twe_beta)
UNION
select sedol 'matchkey', name, sedol 'cusip_sedol', NULL 'cap', 'United Kingdom' 'country', asof
from uke_beta
where asof=(select max(asof) from uke_beta)
UNION
select cusip8 'matchkey', name, cusip8 'cusip_sedol', NULL 'cap', 'USA' 'country', asof
from use_beta 
where asof=(select max(asof) from use_beta)
order by 'matchkey' ASC, cap DESC
HERE

# Step 3 - post-process the temp file
sed -f ${SCRIPTDIR}/match.sed < ${TMPFILE1} > ${TMPFILE2}

awk -f ${SCRIPTDIR}/ipid_barra.awk ${TMPFILE2} > ${TMPFILE3}

# Step 4 - drop the old id table - don't check for error
# as it may not exist
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
drop table ${TABLENAME}
go
HERE

# Step 5 - create the new table
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
create table ${TABLENAME} (
	matchkey varchar(18) NULL,
	name varchar(128) NULL,
        sedol_cusip varchar(16) NULL,
	mktcap money NULL,
        country varchar(64) NULL,
        asof datetime NULL
)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 6 - bcp in the select results
echo "bcp ${TABLENAME} in ${TMPFILE3} /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /f${FORMAT_FILE} /b100"

bcp ${TABLENAME} in ${TMPFILE3} /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /f${FORMAT_FILE} /b100
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
create index ${TABLENAME}_02 on ${TABLENAME}
	(matchkey)
GO
create index ${TABLENAME}_03 on ${TABLENAME}
	(name)
GO
HERE

# Step 7 - drop the old barra mktcap table
# as it may not exist
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
drop table ${MKTCAP_TABLENAME}
go
HERE

# Step 8 - create the new mktcap table
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
create table ${MKTCAP_TABLENAME} (
	xlsid int NULL,
	mktcap money NULL
)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 9 - Populate the table
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
insert into ${MKTCAP_TABLENAME} 
select c.id, MAX(i.mktcap)
from company c, ipid_barra i, security s, security_map m
where
c.id = s.issuer
and
s.id = m.id
and
m.source=4
and
m.sourcekey=i.matchkey
group by c.id
GO
HERE


# Step 8 - clean up
#rm -f ${TMPFILE1} ${TMPFILE2} ${TMPFILE3}
