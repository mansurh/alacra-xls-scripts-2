TITLEBAR="Fetch daily matching and scoring jobs"

day=`date +%a`

x_monthday=`date +%d`
monthday=`expr $x_monthday + 0`

XLSUTILS=$XLS/src/scripts/loading/dba/
. $XLSUTILS/get_registry_value.fn

LOADDIR=${XLSDATA}/matcher
SCRIPTDIR=$XLS/src/scripts/matching/matcher

error_emaillist="john.willcox@alacra.com,carlos.azeglio@alacra.com,isrequests@alacra.com"

# Remove the log file
mkdir -p ${LOADDIR}/log
updatelog=${LOADDIR}/log/fetch_update`date +%Y%m%d`.log

now=`date`
echo "Start Time: ${now}"

echo "Begin fetching daily matching and scoring jobs for ${day} (${monthday})" > ${updatelog}

# Fetch the jobs
${SCRIPTDIR}/fetch_main.sh ${day} ${monthday} ${error_emaillist} >> ${updatelog}
returncode=$?

if [ $returncode != 0 ]
then
     # Mail the results to the administrators
     blat ${updatelog} -t "${error_emaillist}" -s "Fetching daily matching and scoring jobs - FAILURE"
     echo "END Fetching daily matching and scoring jobs - FAILURE" >> ${updatelog}
     now1=`date`
     echo "End Time: ${now1}"
     exit $returncode
fi

echo "END Fetching daily matching and scoring jobs - SUCCESS" >> ${updatelog}
now1=`date`
echo "End Time: ${now1}"

exit $returncode
