TITLEBAR="Extrnal Matching job"

if [ $# != 2 ]
then
    echo "Usage: extmatch_daily.sh id error_emaillist"
    echo "Example: extmatch_daily.sh 1 administrators@alacra.com"
    exit 1
fi

id=$1
error_emaillist=$2

LOADDIR=${XLSDATA}/extmatcher
SCRIPTDIR=$XLS/src/scripts/matching/matcher


# Remove the log file
mkdir -p ${LOADDIR}/log
updatelog=${LOADDIR}/log/extmatch_update`date +%Y%m%d`_${id}.log

now=`date`
echo "Start Time: ${now}"

echo "Begin external matching job ${id}" > ${updatelog}

# Perform the update
${SCRIPTDIR}/extmatch_daily_main2.sh ${id} ${error_emaillist} >> ${updatelog}
returncode=$?

if [ $returncode != 0 ]
then
     echo "END Extrnal Matching job ${id} - FAILURE" >> ${updatelog}
     now1=`date`
     echo "End Time: ${now1}"
     exit $returncode
fi

echo "END External Matching job ${id} - SUCCESS" >> ${updatelog}
now1=`date`
echo "End Time: ${now1}"

exit $returncode