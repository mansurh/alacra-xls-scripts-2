if [ $# != 3 ]
then
    echo "Usage: fetch_main.sh day monthday error_emaillist"
    echo "Example: fetch_main.sh Mon 3 administrators@alacra.com"
    exit 1
fi

day=$1
monthday=$2
error_emaillist=$3

LOADDIR=${XLSDATA}/matcher
SCRIPTDIR=$XLS/src/scripts/matching/matcher
joblog=${LOADDIR}/jobs.log

XLSUTILS=$XLS/src/scripts/loading/dba/
. $XLSUTILS/get_registry_value.fn
. $XLSUTILS/check_return_code.fn

db=concordance
user=`get_xls_registry_value ${db} user`
password=`get_xls_registry_value ${db} password`
server=`get_xls_registry_value ${db} server`

start=`date`
echo "${start}: BEGIN MAIN"
		
# first delete old job details
sql="DELETE FROM pending_jobs WHERE server IS NOT NULL AND DATALENGTH(server) > 0 ";
isql -U${user} -P${password} -S${server} -Q"${sql}" -h-1 -n -b -w"1000"
returncode=$?
if [ $returncode != 0 ]
then
     updatelog=${LOADDIR}/log/fetch_update`date +%Y%m%d`.log
     # Mail the results to the administrators
     blat ${updatelog} -t "${error_emaillist}" -s "Error deleting completed matching and scoring jobs - FAILURE"
     echo "END Deleting completed matching and scoring jobs - FAILURE"
fi

# pull match job details into pending_jobs table
sql="SET NOCOUNT ON INSERT INTO pending_jobs (jobtype,id,jobcategory,timestamp) SELECT 'match','match'+CAST(m.id AS VARCHAR),m.jobcategory,getdate() FROM match_jobs m WHERE m.updateday='${day}' OR m.updateday='${monthday}' OR m.updateday='day'"
isql -U${user} -P${password} -S${server} -Q"${sql}" -h-1 -n -b -w"1000"
returncode=$?
if [ $returncode != 0 ]
then
     updatelog=${LOADDIR}/log/fetch_update`date +%Y%m%d`.log
     # Mail the results to the administrators
     blat ${updatelog} -t "${error_emaillist}" -s "Fetching daily matching jobs - FAILURE"
     echo "END Fetching daily matching jobs - FAILURE"
fi

# pull score job details into pending_jobs table
sql="SET NOCOUNT ON INSERT INTO pending_jobs (jobtype,id,jobcategory,timestamp) SELECT 'score','score'+CAST(s.id AS VARCHAR),s.jobcategory,getdate() FROM score_jobs s WHERE s.updateday='${day}' OR s.updateday='${monthday}' OR s.updateday='day'"
isql -U${user} -P${password} -S${server} -Q"${sql}" -h-1 -n -b -w"1000"
returncode=$?
if [ $returncode != 0 ]
then
     updatelog=${LOADDIR}/log/fetch_update`date +%Y%m%d`.log
     # Mail the results to the administrators
     blat ${updatelog} -t "${error_emaillist}" -s "Fetching daily scoring jobs - FAILURE"
     echo "END Fetching daily scoring jobs - FAILURE job ${id}"
fi
