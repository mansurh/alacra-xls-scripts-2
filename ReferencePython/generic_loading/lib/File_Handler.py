import sys
import os
import pyodbc
from ftplib import FTP
import os.path
import subprocess as sproc
import logging
import time
from shutil import copyfile

class File_Handler(object):
	source = None
	regDir = None
	scriptdir = None
	loaddir = None
	file_name = None
	table_name = None
	today = time.strftime('%Y%m%d')
	now = time.strftime('%Y-%m-%d-%H')
	
	def __init__(self, source, regDir, table_name, file_name):
		self.source = source
		self.regDir = regDir
		self.file_name = file_name
		self.table_name = table_name
		self.scriptdir = os.environ.get('XLS') + '/src/scripts/loading/' + regDir + '/'
		self.loaddir = os.environ.get('XLSDATA') + '/' + regDir + '/'
		logging.basicConfig(filename='logger_bad.log', level=logging.DEBUG, 
		format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p: ')
		
	def create_load_directory(self):
		unix_command = 'mkdir -p ' + self.loaddir + 'log'
		com = os.popen(unix_command)
		print(unix_command)
		unix_command = 'mkdir -p ' + self.loaddir + 'back/' + self.today
		com = os.popen(unix_command)
		print(unix_command)
		
	def rename_files(self, file_name):
		index = file_name.rfind('.')
		suffix = file_name[index:] # error check for no '.'
		print(suffix)
		back_file_name = file_name[:index] + '_bak' + suffix
		print(back_file_name)
		new_file_name = file_name
		if not('_new' in new_file_name):
			new_file_name = file_name[:index] + '_new' + suffix
		# original file -> back file
		source = self.loaddir + file_name
		destination = self.loaddir + 'back/' + self.today + '/' + back_file_name
		copyfile(source, destination)
		# new file -> original file, afterwards delete new file because it is the original 
		unix_command = 'mv ' + self.loaddir + new_file_name + ' ' + self.loaddir + file_name
		os.popen(unix_command)
		
	def store_log(self):
		source = 'logger.log'
		destination = self.loaddir + 'log/' + self.now + '_logger.log'
		copyfile(source, destination)
		
	def check_file_size(self, file_name, threshold):
		#self.file_name = file_name
		unix_command = '''wc -l ''' + self.loaddir + file_name + ''' |cut -f1 -d" "'''
		row_count = os.popen(unix_command).read()
		print(row_count)
		row_count.strip('\n')
		try:
			if int(row_count) < threshold:
				logging.warning(file_name + " does not meet the row threshold of " + str(threshold) + ".")
				print(file_name + " does not meet the row threshold of " + str(threshold) + ".")
				print("Now exiting...")
				sys.exit()
		except ValueError:
			logging.error('ValueError: check_file_size: '+file_name+' row_count was unable to be converted to an int')
			print("ValueError: row_count was unable to be converted to an int")
			print("Exiting system...")
			sys.exit()
		return int(row_count)
		
	def load_data(self, database, creds, delimiter, choice, is_zip):
		table_name = self.table_name
		if is_zip == True and choice == 'update':
			table_name += '_new'
		substring_fname = '.' + self.file_name.split('.',1)[1]
		file_name_new = self.file_name.split('.',1)[0] + '_new'
		file_name_err = file_name_new + '.err'
		file_name_new = file_name_new + substring_fname
		unix_command = 'bcp ' + database + '..' + table_name + ' in ' + self.loaddir + self.file_name + ' -U' + creds['dba_user'] + ' -P' + creds['dba_pass'] + ' -S' + creds['dba_server'] + ' -c -t"' + delimiter + '" -CRAW -e ' + file_name_err
		print('bcp below___________-------------____________-------------'+self.file_name)
		print(os.popen(unix_command).read())
		#command.read()
		
	def find_deviation(self, db_conn, max_deviation):
		cursor = db_conn.cursor()
		sql_command = 'select count(*) from ' + self.table_name
		sql_command_new = 'select count(*) from ' + self.table_name + '_new'
		cursor.execute(sql_command)
		row_count = cursor.fetchone()[0]
		cursor.execute(sql_command_new)
		row_count_new = cursor.fetchone()[0]
		print(self.file_name)
		print(row_count)
		print(row_count_new)
		diff = None
		try:
			if row_count_new >= row_count:
				diff = 1 - (row_count/row_count_new)
			else:
				diff = 1 - (row_count_new/row_count)
			if diff >= (max_deviation/100):
				logging.warning('Max deviation passed. Exiting system.')
				sys.exit()
		except ZeroDivisionError:
			logging.error("ZeroDivisionError: Original table does not exist, try create instead of update.")
			print("ZeroDivisionError: Original table does not exist, try create instead of update.")
			sys.exit()
	