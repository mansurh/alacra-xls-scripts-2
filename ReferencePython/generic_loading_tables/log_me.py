import pyodbc
import subprocess as sproc
import sys
from datetime import date, datetime
import time
import os
import traceback
from os import listdir
from os.path import isfile, join, basename, splitext

"""
"""
class logger(object):
	filename = None
	path = None
	destination = None
	log_file = None
	error_file = None
	today = time.strftime('%Y_%m_%d')
	now = time.strftime('%H:%M:%S')
	last_msg = None
	stage = None
	
	def __init__(self,filename,path,destination,stage):
		self.filename = filename
		self.path = path
		if len(self.path) > 0 and self.path[-1] != '/': self.path += '/'
		self.destination = destination
		if len(self.destination) > 0 and self.destination[-1] != '/': self.destination += '/'
		self.create_log()
		self.stage = stage
		
	def create_log(self):
		self.log_file = open(self.path + self.filename, 'w')
		self.log_file.write('Log file created on ' + self.today + ' at ' + self.now + '\n')
		
	def create_error_file(self):
		self.error_file = open(self.path + splitext(self.filename)[0]+self.today+'_bad_log.txt', 'w')
		self.error_file.write('Bad log file created on ' + self.today + ' at ' + self.now + '\n\n')
		if self.last_msg == None: 
			self.error_file.write('Nothing was logged in the succesful log file before this error occured.\n\n')
		else:
			self.error_file.write('The last succesful log entry was: ' + self.last_msg + '\n\n')
	
	def info(self, msg):
		self.now = time.strftime('%H:%M:%S')
		self.log_file.write('Success @ ' + self.now + ': ' + msg + '\n')
		self.log_file.flush()
		self.last_msg = msg
		
	def warning(self, msg):
		self.now = time.strftime('%H:%M:%S')
		self.log_file.write('Warning @ ' + self.now + ': ' + msg + '\n')
		self.log_file.flush()
		self.last_msg = msg
	
	def error(self, msg):
		self.now = time.strftime('%H:%M:%S')
		self.create_error_file()
		self.error_file.write('Error @ ' + self.now + ': ' + msg + '\n')
		self.error_file.flush()
		time.sleep(2)
		#self.notifyBad()
		time.sleep(2)
		self.end_fail()
		time.sleep(2)
		sys.exit(1)
		
	"""
	"""
	def notifyBad(self):
		self.error_file.close()
		if self.stage != 'test':
			unix_cmd = 'blat ' + self.path+splitext(self.filename)[0]+self.today+'_bad_log.txt' + ' -t matthew.pitcher@alacra.com'+' -f "administrators@alacra.com" -s "ERROR: Delivery Failed - '+self.filename+' - '+self.today+' "'
			print(os.popen(unix_cmd).read())
	
	def end_success(self):		
		if self.destination != None:
			unix_command = 'touch ' + self.path + self.filename 
			print(os.popen(unix_command).read())
			time.sleep(1)
			
			unix_command = 'mv ' + self.path + self.filename + ' ' + self.destination + splitext(self.filename)[0] + self.today + splitext(self.filename)[1]
			print(os.popen(unix_command).read())
			time.sleep(1)
			
			
	
	def end_fail(self):
		self.end_success()
		if self.destination != None:
			unix_command = 'touch ' + self.path + splitext(self.filename)[0]+self.today+'_bad_log.txt'
			os.popen(unix_command)
			time.sleep(1)
			
			unix_command = 'mv ' + self.path + splitext(self.filename)[0]+self.today+'_bad_log.txt ' + self.destination + splitext(self.filename + '_bad')[0] + self.today + '_bad.log'
			os.popen(unix_command)
			time.sleep(1)

	
	
	
	