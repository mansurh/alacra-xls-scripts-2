exec FAMTREE_python_proc
------------------------
CREATE PROCEDURE FAMTREE_python_proc
AS            
BEGIN            
        
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FAMTREE_python_table]') AND type in (N'U'))        
DROP TABLE [dbo].[FAMTREE_python_table]            
      
create table FAMTREE_python_table (      
[org_id] int,
[parent_id] int,
[ultimate_parent] int 
)      
      
insert into FAMTREE_python_table([org_id])      
select distinct Organization_ID  
from CFG_moodysload_organization_rating_root     

 
update FAMTREE_python_table     
set [parent_id] = CFG_moodysload_organization_rating_root.Organization_Parent_Number     
from CFG_moodysload_organization_rating_root     
where CFG_moodysload_organization_rating_root.Organization_ID = [org_id]      


update FAMTREE_python_table     
set [ultimate_parent] = CFG_moodysload_organization_rating_root.Organization_Ultimate_Parent_Number     
from CFG_moodysload_organization_rating_root     
where CFG_moodysload_organization_rating_root.Organization_ID = [org_id]  