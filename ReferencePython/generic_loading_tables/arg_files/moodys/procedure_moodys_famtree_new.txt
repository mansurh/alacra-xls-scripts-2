exec moodys_famtree_proc
------------------------
Create PROCEDURE [dbo].[moodys_famtree_proc]  
AS          
BEGIN          
      
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[moodys_famtree_python]') AND type in (N'U'))      
DROP TABLE [dbo].[moodys_famtree_python]          
    
create table moodys_famtree_python (    
[Moodys Issuer Number] varchar(20),    
[Organization Name] varchar(255),    
[Organization Parent Number] varchar(20),    
[Organization Parent Name] varchar(255),    
[Organization Ultimate Parent Number] varchar(20),    
[Organization Ultimate Parent Name] varchar(255)
)    
    
    
insert into moodys_famtree_python([Moodys Issuer Number])    
select distinct Organization_ID
from CFG_moodysload_organization_rating_root   
    
    
update moodys_famtree_python    
set [Organization Name] = CFG_moodysload_organization_rating_root.Moodys_Legal_Name   
from  CFG_moodysload_organization_rating_root   
where  CFG_moodysload_organization_rating_root.Organization_ID = [Moodys Issuer Number]     
    
    
update moodys_famtree_python    
set [Organization Parent Number] = CFG_moodysload_organization_rating_root.Organization_Parent_Number   
from CFG_moodysload_organization_rating_root   
where CFG_moodysload_organization_rating_root.Organization_ID = [Moodys Issuer Number]    
    
    
update moodys_famtree_python    
set [Organization Parent Name] = CFG_moodysload_organization_rating_root.Organization_Parent_Name   
from CFG_moodysload_organization_rating_root    
where CFG_moodysload_organization_rating_root.Organization_ID = [Moodys Issuer Number]    
    
    
update moodys_famtree_python    
set [Organization Ultimate Parent Number] = CFG_moodysload_organization_rating_root.Organization_Ultimate_Parent_Number   
from CFG_moodysload_organization_rating_root   
where CFG_moodysload_organization_rating_root.Organization_ID = [Moodys Issuer Number]    
    
    
update moodys_famtree_python    
set [Organization Ultimate Parent Name] = CFG_moodysload_organization_rating_root.Organization_Ultimate_Parent_Name 
from CFG_moodysload_organization_rating_root    
where CFG_moodysload_organization_rating_root.Organization_ID = [Moodys Issuer Number]    
    
    
    
END 