org_id|varchar(12)
ticker|varchar(255)
name|varchar(255)
country|varchar(51)
broad_business_line|varchar(51)
specific_business_line|varchar(51)
specific_SIC|varchar(12)
specific_SIC_text|varchar(51)
broad_SIC|varchar(11)
broad_SIC_text|varchar(51)
industry|varchar(51)