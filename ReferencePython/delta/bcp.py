import argparse
import sys
import os
import time
from os import listdir
from os.path import isfile, join

def main():
	#add a client arg ubs || fitch
	client = 'fitch'#'ubs2' # 'ubs'
	
	dirs = ['09','10','11','12','13','14','15','16','17','18','19','20']
	times = ['0300','1100','1900']
	start_year = 2017
	start_month = 1
	start_day = 1
	start_date = str(start_year)+str(start_month)+format(start_day,'02d')
	end_date = '20170412'
	end_year = int(end_date[0:4])
	end_month = int(end_date[4:6])
	end_day = int(end_date[6:8])
	print(end_year)
	print(end_month)
	print(end_day)
	unix_command = "mkdir -p bcp"
	print(os.popen(unix_command).read())
		
	if client == 'fitch':#'do not do this':#'ubs2':
		dates = []
		unix_command = "mkdir -p fitch"
		months = {1:31,2:28,3:31,4:31,5:30,6:31,7:30,8:31,9:30,10:31,11:30,12:31}
		while start_year <= end_year and not (start_year == end_year and start_month > end_month):
			while start_day <= months[start_month]:
				start_date = str(start_year)+format(start_month,'02d')+format(start_day,'02d')
				print(start_date)
				dates.append(start_date)
				unix_command = 'wget http://data34/xlsdata/aafClientXfers/fitch/back/output/'+start_date+'/fitch_extract_5_identifier_delta_'+start_date+'.txt -P fitch'
				#unix_command = 'wget http://data34/xlsdata/ubs_lem/back/output/'+start_date+'/aafUBS_identifier_2017-'+format(start_month,'02d')+'-'+format(start_day,'02d')+'_0300.txt.gz -O "c:/users/xls/src/scripts/referencepython/delta/bcp/aafUBS_identifier_2017-'+format(start_month,'02d')+'-'+format(start_day,'02d')+'_0300.txt.gz"'
				print(os.popen(unix_command).read())				
				#unix_command = 'wget http://data34/xlsdata/ubs_lem/back/output/'+start_date+'/aafUBS_identifier_2017-'+format(start_month,'02d')+'-'+format(start_day,'02d')+'_1100.txt.gz -O "c:/users/xls/src/scripts/referencepython/delta/bcp/aafUBS_identifier_2017-'+format(start_month,'02d')+'-'+format(start_day,'02d')+'_1100.txt.gz"'
				#print(os.popen(unix_command).read())
				#unix_command = 'wget http://data34/xlsdata/ubs_lem/back/output/'+start_date+'/aafUBS_identifier_2017-'+format(start_month,'02d')+'-'+format(start_day,'02d')+'_1900.txt.gz -O "c:/users/xls/src/scripts/referencepython/delta/bcp/aafUBS_identifier_2017-'+format(start_month,'02d')+'-'+format(start_day,'02d')+'_1900.txt.gz"'
				#print(os.popen(unix_command).read())
				start_day += 1
			if start_month < 12: 
				start_month += 1
			else: 				
				start_month = 1
				start_year += 1
			start_day = 1
		time.sleep(10)
		#UBS...unix_command = 'gunzip c:/users/xls/src/scripts/referencepython/delta/bcp/*.gz'
		#UBS...print(os.popen(unix_command).read())
		#UBS...print('\n\ngunzip\n\n')
		#UBS...time.sleep(5)	
		
		#for date in dates:
			#unix_command = 'bcp concordance..fitch_deltas in fitch/'+date+'.bcp -Uconcordancedbo -Pconcordancedbo -Sdatatest4 -c -t"|" -CRAW -e "errorfile'+date+'.err" -F2'
			#print(os.popen(unix_command).read())	
		#sys.exit()

		
	mypath = 'c:/users/xls/src/scripts/referencepython/delta/fitch/'
	onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
	print(onlyfiles)
	bcp_file = open('bcp.txt','w')
	for file in onlyfiles:
		old = open('fitch/'+file,'r',encoding='UTF-8', errors='ignore')
		lines = old.readlines()
		for line in lines[1:]:
			line = line.replace('\n','').replace('\r','') + '|' + file.split('_')[5][0:4] + "-" + file.split('_')[5][4:6] + "-" + file.split('_')[5][6:8] + '\r\n' #file.split('_')[2] for UBS
			bcp_file.write(line)
		print(file)
		
	sys.exit()
		
		
	if client == 'ubs':
		files = {}
		for dir in dirs:
			files[dir] = []
			for t in times:
				filename = "aafUBS_identifier_2017-01-"+dir+"_"+t+".txt"
				try:
					file = open(dir+'/'+filename,'r')
					file2 = open('bcp/'+dir+'_'+t+'.txt','w')
					files[dir].append(dir+'_'+t+'.txt')
					for line in file.readlines():
						file2.write(line.strip('\n'))
						file2.write('\r\n')
				except:
					print("file not found "+filename)
					pass
					
		count=0
		for dir in files:
			dir_files = files[dir]
			for file in dir_files:
				unix_command = 'bcp concordance..aafUBS_identifier_2017_01_'+dir+' in bcp/'+file+' -Uconcordancedbo -Pconcordancedbo -Sdatatest4 -c -t"\t" -CRAW -e "errorfile'+str(count)+'.err" -F2'
				count+=1
				print(os.popen(unix_command).read())


if __name__ == '__main__':
	main()