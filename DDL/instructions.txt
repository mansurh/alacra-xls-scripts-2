Instructions to run job on ddl (mail/ftp) servers for incoming email:


1. All of the ddl (mail/ftp) servers scripts should be located in /home/scripts/<user> directory.
2. <user> should be part of <ftp> and <scripts> groups.  All of the scripts for the <user> should be in /home/scripts/<user> directory, 
where all of the files uploaded or downloaded by users should be in /home/ftp/<user> (which should also be the home directory for <user>, and
where .forward file, if any, reside).
3. sendmail requires that all of the scripts, which run via .forward file, are located in /etc/smrsh directory
4. Since the scripts are located in /home/scripts/<user> directory, we should create a symbolic link between the /etc/shrsh directory and the script.
For example, if you have a script in /home/scripts/scotland directory, with the name create_url, then create symbolic link such as:
ln -s /home/scripts/scotland/create_url scotland_create_url

5. To run this script when an email arrives, put the following in .forward file (make sure you put double-quotes around pipe and the file):
"| scotland_create_url"

6. If you'd like the servers to save the email, put \<user> on the first line of .forward, such as:
\scotland


