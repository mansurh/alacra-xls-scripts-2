scriptname=$0
echo "DO NOT RUN WITHOUT PERMISSION FROM Information Systems personnel!!"
grep -q "^at " ${scriptname}
if [ $? -eq 0 ]
then
  echo "ERROR: Old command (at.exe) is used, no longer supported"
  echo "ERROR: No action taken, review the changes, and switch to SCHTASKS command"
  exit 1
fi

# Define the scheduling server
schedserver=XLSSTAGE2
if [ $COMPUTERNAME != $schedserver ]
then
    echo "Not $schedserver, exiting..."
    exit 1
fi

run()
{

#catch if any of these fail
set -ex
#SCHTASKS /F /Delete /TN "*"


!!!!!!! DO NOT USE THIS FILE ANYMORE  !!!!
!!!	For EU data centers, use jobstreams/atlist_eu.sh
!!! For US-Rackspace data centers, use jobstreams/rackspace/atlist_rackspace.sh
!!! For Test systems, use jobstreams/atlisttest.sh

}

rm -f *.log
logfile=atlist`date +%m%d`.log
echo "Logfile is: $logfile"
(run) > $logfile 2>&1
rc=$?

#Get Number of jobs listed in this file, and the number loaded into scheduler
typeset -i loaded
typeset -i listed
loaded=`SCHTASKS /query /NH |grep -v "^$"|wc -l`
listed=`egrep -i "^(SCHTASKS /Create|at) " ${scriptname}|grep -iv " /delete"|wc -l`

if [ $rc -ne 0 ]
then
  #SCHTASKS /F /Delete /TN "*" > /dev/null
  echo
  echo
  echo
  echo "LAST JOB FAILED TO LOAD.  LOADED ONLY $loaded JOBS OUT OF $listed LISTED IN ${scriptname}"
  echo "TAKE A LOOK AT ITS CALL, AND TRY TO FIX IT"
  echo "Log file name is $logfile"
elif [ $loaded -ne $listed ]
then
  #If number of jobs loaded is different from listed, report a problem
  #This could happen if the same job is listed more than once for SCHTASKS
  echo
  echo
  echo "POSSIBLE ERRORS IN LOADING JOBS"
  echo "Number of jobs loaded ($loaded) mismatched number of listed jobs ($listed)."
  echo "All other jobs are scheduled to run, please review the difference for possible problems."
  echo "Log file name is $logfile"
else
  echo "$loaded jobs loaded successfully"
  echo "Do you want to inspect the log file?(Y/N)"
  read zzz
  if [ "$zzz" = "y" -o "$zzz" = "Y" ]
  then
    echo "Log file name is $logfile"
  else
    rm -f $logfile
  fi
fi

exit 0
