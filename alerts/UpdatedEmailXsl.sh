sslogon="skarra"
sspsw="skarra"

# set the 'ignore prompts' flag (useful to remove for debugging)
iflag=-I-N


# Do for all the files in the RCS directory
rcsroot=c:/usr/netscape/server/docs/alerts
acctlist=""
for filename in `ls ${rcsroot}`
do
	# See if this is a directory
	if [ -d ${rcsroot}/${filename} ]
	then
		if [ "${filename}" != "6720" ]
		then
			cmp -s c:/usr/netscape/server/docs/alerts/6720/emailalert_old.xsl ${rcsroot}/${filename}/emailalert.xsl
			if [ $? = 0 ]
			then
				cmp -s c:/usr/netscape/server/docs/alerts/6720/emailmobile_old.xsl ${rcsroot}/${filename}/emailmobile.xsl
				if [ $? = 0 ]
				then
					acctlist="${acctlist} ${filename}"
				fi
			fi
		fi
	fi
done

echo "acctlist: ${acctlist}"


#exit 0


#acctlist="6751"


for acct in $acctlist
do

	newdir=docs/alerts/${acct}
	if [ ! -d ${newdir} ]
	then

	# convert all forward slashes to backslashes
	fwslashpath1=$(sed -e"s/\//\\\\/g" << HERE
${newdir}
HERE
)

		ssdir="\$/xls/${newdir}"
		physdir="c:/usr/netscape/server/${newdir}"
		physdirdos="c:\\usr\\netscape\\server\\${fwslashpath1}"

		#if [ ! -d ${physdir} ]
		#then
		#	echo "creating ${physdir}"
		#	mkdir -p ${physdir}
		#	if [ $? != 0 ]
		#	then
		#		echo "Error - could not create ${physdir}"
		#		exit 1
		#	fi
		#	# create the new project
		#	echo "ss Create ${ssdir} ${iflag} -Y${sslogon},${sspsw}"
		#	ss Create ${ssdir} ${iflag} -Y${sslogon},${sspsw}
		#fi

		echo "cd ${physdir}"
		cd ${physdir}

		# set the project under alerts to be the current one
		echo "ss Cp ${ssdir} ${iflag} -Y${sslogon},${sspsw}"
		ss Cp ${ssdir} ${iflag} -Y${sslogon},${sspsw}

		# check out the files
		echo "ss Checkout emailalert.xsl ${iflag} -Y${sslogon},${sspsw}"
		ss Checkout emailalert.xsl ${iflag} -Y${sslogon},${sspsw}

		echo "ss Checkout emailmobile.xsl ${iflag} -Y${sslogon},${sspsw}"
		ss Checkout emailmobile.xsl ${iflag} -Y${sslogon},${sspsw}


		# overwrite
		echo "cp -p c:/usr/netscape/server/docs/alerts/6720/emailalert.xsl ${physdir}/emailalert.xsl"
		cp -p c:/usr/netscape/server/docs/alerts/6720/emailalert.xsl ${physdir}/emailalert.xsl

		echo "cp -p c:/usr/netscape/server/docs/alerts/6720/emailmobile.xsl ${physdir}/emailmobile.xsl"
		cp -p c:/usr/netscape/server/docs/alerts/6720/emailmobile.xsl ${physdir}/emailmobile.xsl


		# check in the files
		echo "ss Checkin email*.xsl -P ${iflag} -Y${sslogon},${sspsw}"
		ss Checkin email*.xsl -P ${iflag} -Y${sslogon},${sspsw}

		##echo "ss Checkin ""${physdirdos}\\emailmobile.xsl"" ${iflag} -Y${sslogon},${sspsw}"
		##ss Checkin "${physdirdos}\\emailmobile.xsl" ${iflag} -Y${sslogon},${sspsw}

	fi

done

exit 0
