sslogon=skarra
sspsw=skarra

# set the 'ignore prompts' flag (useful to remove for debugging)
iflag=-I-N


declare -i i
declare -i acct
i=0
acct=6965
while [ $i -lt 50 ]
do
#	echo "$i, $acct"

	newdir=docs/alerts/${acct}
	if [ ! -d ${newdir} ]
	then

	# convert all forward slashes to backslashes
	fwslashpath1=$(sed -e"s/\//\\\\/g" << HERE
${newdir}
HERE
)

		ssdir="\$/xls/${newdir}"
		physdir="c:/usr/netscape/server/${newdir}"
		physdirdos="c:\\usr\\netscape\\server\\${fwslashpath1}"

		# create the new project
		echo "ss Create ${ssdir} ${iflag} -Y${sslogon},${sspsw}"
		ss Create ${ssdir} ${iflag} -Y${sslogon},${sspsw}

		if [ ! -d ${physdir} ]
		then
			echo "creating ${physdir}"
			mkdir -p ${physdir}
			if [ $? != 0 ]
			then
				echo "Error - could not create ${physdir}"
				exit 1
			fi
		fi

		cd ${physdir}

		cp c:/usr/netscape/server/docs/alerts/6720/emailalert.xsl ${physdir}
		cp c:/usr/netscape/server/docs/alerts/6720/emailmobile.xsl ${physdir}
		chmod +r+w ${physdir}/email*.xsl

		# set the new project under alerts to be the current one
		echo "ss Cp ${ssdir} ${iflag} -Y${sslogon},${sspsw}"
		ss Cp ${ssdir} ${iflag} -Y${sslogon},${sspsw}

		# add the files
		echo "ss Add emailalert.xsl ${iflag} -Y${sslogon},${sspsw}"
		ss Add emailalert.xsl ${iflag} -Y${sslogon},${sspsw}
		echo "ss Add emailmobile.xsl ${iflag} -Y${sslogon},${sspsw}"
		ss Add emailmobile.xsl ${iflag} -Y${sslogon},${sspsw}



		ssdir="\$/xls/docs/EmailCfg"
		physdir="c:/usr/netscape/server/docs/EmailCfg"
		physdirdos="c:\\usr\\netscape\\server\\EmailCfg"

		cd ${physdir}

		if [ ! -e ${physdir}/${acct}.xml ]
		then
			cp ${physdir}/6720.xml ${physdir}/${acct}.xml
			chmod +r+w ${physdir}/${acct}.xml
		fi

		# set Emailcfg project to be the current one
		echo "ss Cp ${ssdir} ${iflag} -Y${sslogon},${sspsw}"
		ss Cp ${ssdir} ${iflag} -Y${sslogon},${sspsw}

		# add the file
		echo "ss Add ${acct}.xml ${iflag} -Y${sslogon},${sspsw}"
		ss Add ${acct}.xml ${iflag} -Y${sslogon},${sspsw}

	fi


	let i=i+1
	let acct=acct+1
done

exit 0
