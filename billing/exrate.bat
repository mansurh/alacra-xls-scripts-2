set -x

if "%1" == "" goto ERROR

if "%2" == "" goto QUERY
echo update currency set exchange=%2 where code="%1" > cmdfile
echo select "NEW RATE=",exchange from currency where code="%1" >> cmdfile
isql /Uxls /Pxls /Salaxls /s" " /w1000 < cmdfile
goto END

:QUERY
echo select "EXISTING RATE=",exchange from currency where code="%1" > cmdfile
isql /Uxls /Pxls /Salaxls /s" " /w1000 < cmdfile
goto END

:ERROR
echo "usage: exrate [GBP|EUR] [rate]"

:END
