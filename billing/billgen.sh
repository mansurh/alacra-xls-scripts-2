XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# Parse the command line arguments
#	1 - database
#	2 - month back
if [ $# -lt 1 ]
then
	echo "Usage: billcheck database [month back]"
	exit 1
fi

database=$1
monthback=1
if [ $# -gt 1 ]; then monthback=$2; fi;


emaillist="accounting@opus.com,tami.sitman@opus.com"

# Save the runstring parameters
logon=`get_xls_registry_value ${database} User`
password=`get_xls_registry_value ${database} Password`
server=`get_xls_registry_value ${database} Server`

regkey=billing
ftpserver=`get_xls_registry_value ${regkey} Server 6`
ftpuser=`get_xls_registry_value ${regkey} User 6`
ftppass=`get_xls_registry_value ${regkey} Password 6`

#set output file names
INVOICE=`date -d "$month month ago" +%Y%m`
MONTHNAME=`date -d "$month month ago" +%B`
YEAR=`date -d "$month month ago" +%Y`
JOURNALFILE=journal${MONTHNAME}${YEAR}.txt
DUPEFILE=dupes${MONTHNAME}${YEAR}.txt
ARCHFILE=${MONTHNAME}${YEAR}.zip
logfile=$XLSDATA/billgen/log/billgen${INVOICE}.log
cmdfile=billgen.ftp

main(){
  touch ${JOURNALFILE} ${DUPEFILE} 
  rm -rf *.xls ${cmdfile}

  osql -b -U ${logon} -P${password} -S ${server}  /Q "exec remove_duplicate_charges '${MONTHNAME} $YEAR'"
  if [ $? != 0 ]; then echo "error in remove_duplicate_charges '${MONTHNAME} $YEAR' ; exiting..."; return 1; fi;

  bcp "select * from duplicate_charges where runtime > dateadd(day,-1,getdate())" queryout ${DUPEFILE} -U ${logon} -P${password} -S ${server} -c
  if [ $? != 0 ]; then echo "error in getting duplicates' ; exiting..."; return 1; fi;

  billgen.exe -m ${MONTHNAME} -y ${YEAR} -U ${logon} -P ${password} -S ${server} -i ${INVOICE}0000 -I import${JOURNALFILE} -V importV2${JOURNALFILE} > ${JOURNALFILE}
  if [ $? != 0 ]; then echo "error in billgen.exe -m ${MONTHNAME} -y ${YEAR} -i ${INVOICE}0000 -I import${JOURNALFILE} -V importV2${JOURNALFILE}; exiting..."; return 1; fi;

  zip ${ARCHFILE} ${JOURNALFILE} *.xls 
  if [ $? != 0 ]; then echo "error in zip ${ARCHFILE} ${JOURNALFILE} *.xls; exiting..."; return 1; fi;
  
  rm -f ${cmdfile}
  echo "user ${ftpuser} ${ftppass}" > ${cmdfile}
  echo "binary" >> ${cmdfile}
  echo "cd /billing/billing" >> ${cmdfile}
  echo "put ${ARCHFILE}" >> ${cmdfile}
  echo "quit" >> ${cmdfile}
  ftp -i -n -s:${cmdfile} ${ftpserver}
  if [ $? != 0 ]
  then
	echo "FTP error, aborting"
	return 1
  fi

}

mkdir -p $XLSDATA/billgen/log
cd $XLSDATA/billgen
(main) >  ${logfile} 2>&1
if [ $? -ne 0 ]
then
	blat $logfile -t administrators@alacra.com -s "Billgen failed"
	exit 1
else
	blat ${DUPEFILE} -attach ${JOURNALFILE} -attach ${ARCHFILE} -t ${emaillist} -s "Billgen for ${MONTHNAME} ${YEAR}"
fi
