 XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

if [ $# -lt 5 ]
then
	echo "Usage: PulseTrialStats.sh alacralogserver1 alacralogserver2 daysback accountid emailDest"
	exit 1
fi

shname=$0
alacralogserver1=$1
alacralogserver2=$2
daysback=$3
accountid=$4
emailDest=$5


dbname="alacralog"
auser=`get_xls_registry_value ${dbname} "User" "0"`
apassword=`get_xls_registry_value ${dbname} "Password" "0"`

dbname="xls"
xserver=`get_xls_registry_value ${dbname} "Server" "0"`
xuser=`get_xls_registry_value ${dbname} "User" "0"`
xpassword=`get_xls_registry_value ${dbname} "Password" "0"`

dbname="alerts"
tserver=`get_xls_registry_value ${dbname} "Server" "0"`
tuser=`get_xls_registry_value ${dbname} "User" "0"`
tpassword=`get_xls_registry_value ${dbname} "Password" "0"`

tempsuffix=_temp
tempdatafile=temp.dat

reportxmlfile=pulseTrials_`date +%y%m%d`.xml

# --------------------------------

prepare_empty_temp_table()
{
	if [ $# -lt 3 ]
	then
		return 1
	fi
	THESERVER=$1
	THETABLE=$2
	THETEMPTABLE=$3

	echo "Initializing temp table ${THETEMPTABLE}"
	isql /S${THESERVER} /U${auser} /P${apassword} << HERE
if exists (select * from dbo.sysobjects where id = object_id(N'[${THETEMPTABLE}]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) 
begin
	print 'Truncating table ${THETEMPTABLE}'
	truncate table ${THETEMPTABLE}
end
else
begin
	print 'Creating table ${THETEMPTABLE}'
	select * into ${THETEMPTABLE} from ${THETABLE} where 1 = 0
end
GO
HERE
}


merge_data_temp_table()
{
	if [ $# -lt 3 ]
	then
		return 1
	fi
	MYTABLE=$1
	MYTEMPTABLE=${MYTABLE}${tempsuffix}
	MYSTARTTIME=$2
	MYENDTIME=$3

	# Prepare temp table on server1
	prepare_empty_temp_table "${alacralogserver1}" "${MYTABLE}" "${MYTABLE}${tempsuffix}"


	# copy data from server2
	$XLS/src/scripts/transfer/copyselectedrowslow.sh "select * from ${MYTABLE} where eventtime >= '${startdate}' and eventtime < '${enddate}'" alacralog "${alacralogserver2}" "${auser}" "${apassword}" "${MYTABLE}${tempsuffix}" alacralog "${alacralogserver1}" "${auser}" "${apassword}" "${tempdatafile}"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error copying data from ${MYTABLE} table on ${alacralogserver2} to ${alacralogserver1}"
	    return 1
	fi

	# insert the data from server1
	isql /S${alacralogserver1} /U${auser} /P${apassword} << HERE
insert into ${MYTABLE}${tempsuffix}
  select * from ${MYTABLE} where eventtime >= '${startdate}' and eventtime < '${enddate}'
HERE
}


# SCRIPT BEGINNING


run() {


	# get start datetime string
	mysql="set nocount on select convert(varchar(32),dateadd(day,-${daysback},getdate()),101) + ' 00:00:00.0'"
	isql /Q "${mysql}" /S${xserver} /U${xuser} /P${xpassword} -b -n -h-1 -w4096 -s"|" | sed -e "s/\n//g" | grep -v "^$" > date.tmp
	read startdate < date.tmp
	rm date.tmp
	echo "Start date = [$startdate] ..."

	# get end datetime string
	mysql="set nocount on select convert(varchar(32),getdate(),101) + ' 00:00:00.0'"
	isql /Q "${mysql}" /S${xserver} /U${xuser} /P${xpassword} -b -n -h-1 -w4096 -s"|" | sed -e "s/\n//g" | grep -v "^$" > date.tmp
	read enddate < date.tmp
	rm date.tmp
	echo "End date = [$enddate] ..."



	merge_data_temp_table pulseimpressionlog "${startdate}" "${enddate}"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error merging pulseimpressionlog data"
	    return 1
	fi

	merge_data_temp_table pulsesearchlog "${startdate}" "${enddate}"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error merging pulsesearchlog data"
	    return 1
	fi


	# copy login stats from xls

	# empty the temp table (xls_logins_temp)
	prepare_empty_temp_table "${alacralogserver1}" xls_logins xls_logins_temp


	# part 1: copy the user login information for everyone who logged in
	# part 2: append the users who did not log in
	xlsquery="select u.account, l.userid, u.login, u.first_name, u.last_name, u.company, count(*), max(l.time), convert(varchar,u.commence,101), convert(varchar,u.terminate,101), u.email \
from logins l \
join users u on l.userid = u.id \
where u.account IN ( ${accountid} ) \
and u.terminate >= '${startdate}' \
and l.time >= '${startdate}' \
and l.time <  '${enddate}' \
and (project is null or l.remote_address not like '192.168.%') \
group by u.account, l.userid, u.login, u.first_name, u.last_name, u.company, u.commence, u.terminate, u.email \
union all \
select u.account, u.id, u.login, u.first_name, u.last_name, u.company, 0, null, convert(varchar,u.commence,101), convert(varchar,u.terminate,101), u.email \
from users u \
where u.account IN ( ${accountid} ) \
and u.terminate > '${startdate}' \
and u.id not in (select distinct userid from logins where time >= '${startdate}' \
and time <  '${enddate}' and (project is null or remote_address not like '192.168.%') ) "

	$XLS/src/scripts/transfer/copyselectedrowslow.sh "${xlsquery}" xls "${xserver}" "${xuser}" "${xpassword}" xls_logins_temp alacralog "${alacralogserver1}" "${auser}" "${apassword}" "${tempdatafile}"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error copying extracted user data from xls database on ${xserver} to xls_logins_temp table in alacralog on ${alacralogserver1}"
	    return 1
	fi




	# copy alerts stats from alerts

	# empty the temp table (alerts_emails_temp)
	prepare_empty_temp_table "${alacralogserver1}" alerts_emails alerts_emails_temp


	xlsquery="select u.alacrauser_id, count(*), max(alerttime) \
from useralerthistory uah \
join users u on uah.userid = u.[user_id] \
where uah.alerttime >= '${startdate}' \
and uah.alerttime <  '${enddate}' \
and uah.status = 0 \
and uah.userid in ( \
	select userid from user_profiles where saved_profile_id in ( \
		select saved_profile_id From filter_spec where ip = 387 \
	) \
) group by u.alacrauser_id"

	$XLS/src/scripts/transfer/copyselectedrowslow.sh "${xlsquery}" alerts "${tserver}" "${tuser}" "${tpassword}" alerts_emails_temp alacralog "${alacralogserver1}" "${auser}" "${apassword}" "${tempdatafile}"
	if [ $? != 0 ]
	then
	    echo "${shname}: Error copying extracted data from alerts database on ${tserver} to alerts_emails_temp table in alacralog on ${alacralogserver1}"
	    return 1
	fi



	# Run the query for the spreadsheet
	Sheet=pulse_trial

	# convert all forward slashes to backslashes
	fwslashpath1=$(sed -e"s/\//\\\\/g" << HERE
$XLS/src/scripts/reports/pulse/${Sheet}.sql
HERE
)

	echo "sqlcmd -S ${alacralogserver1} -U ${auser} -P ${apassword} -i ${fwslashpath1} -o ${Sheet}.rpt -W -s""|"" -h -1"
	sqlcmd -S ${alacralogserver1} -U ${auser} -P ${apassword} -i ${fwslashpath1} -o ${Sheet}.rpt -W -s"|" -h -1
	if [ $? != 0 ]
	then
		echo "Error running SQLQuery ${Sheet}.sql, terminating"
		return 1
	fi 

	# convert it to xml
	sed -f $XLS/src/scripts/reports/pulse/${Sheet}.sed < ${Sheet}.rpt > ${Sheet}.xml

	# Must be in the UTF-8 Character Set
	iconv -f ISO-8859-1 -t UTF-8 < ${Sheet}.xml > ${Sheet}_utf.xml
	if [ $? != 0 ]
	then
		echo "Error converting output to UTF-8, terminating"
		return 1
	fi 


	# Start the report output file
	cat< $XLS/src/scripts/reports/Weblogdb/preamble.xml >${reportxmlfile}
	if [ $? != 0 ]
	then
		echo "Error creating ${reportxmlfile} file, terminating"
		return 1
	fi

	# Append together the Excel bits
	for bit in $XLS/src/scripts/reports/pulse/${Sheet}Start.xml ${Sheet}_utf.xml $XLS/src/scripts/reports/Weblogdb/WorksheetEnd.xml
	do
		cat <${bit}  >> ${reportxmlfile}
		if [ $? != 0 ]
		then
			echo "Error concatenating ${bit} to report file, terminating"
			return 1
		fi 
	done

	# Clean Up
	rm -f ${Sheet}.rpt
	rm -f ${Sheet}.xml
	rm -f ${Sheet}_utf.xml

	# End the report output file
	cat < $XLS/src/scripts/reports/Weblogdb/postamble.xml >>${reportxmlfile}
	if [ $? != 0 ]
	then
		echo "Error wrapping up ${reportxmlfile} file, terminating"
		return 1
	fi
}


out_dir=$XLSDATA/alacralog
logfile=${out_dir}/PulseTrialStats_`date +%y%m%d`.log


run > ${logfile}
if [ $? -ne 0 ]
then
	blat ${logfile} -t administrators@alacra.com -s "Pulse Trial Usage Script failed."
	exit 1
else
	desc="Pulse Trial Stats: ${startdate} ET - ${enddate} ET"

	# Mail out the report.  Must be a binary attachment as UTF-8 uses the high bit
	blat -to ${emailDest} -subject "${desc}" -body "${desc}.  See Attached." -uuencode -attach ${reportxmlfile}
	if [ $? != 0 ]
	then
		echo "Error sending report file via Blat, terminating"
		exit 1
	fi
fi

exit 0

