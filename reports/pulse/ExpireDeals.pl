use Win32::OLE;
use OLE;

$SERVER=$ARGV[0];
$DATABASE=$ARGV[1];;
$USER=$ARGV[2];
$PASSWORD=$ARGV[3];
$datadir=$ARGV[4];
$QUERY=$ARGV[5];
$file=$ARGV[6];


unless (open(OUTFILE, ">$datadir/$file")){
   die "cannot open $datadir/$file file";
}

#Create connection strings
my $conn=CreateObject OLE "ADODB.Connection" || die "CreateObject: $!";
my $connect="DRIVER={SQL SERVER};PWD=$PASSWORD;UID=$USER;SERVER=$SERVER;DATABASE=$DATABASE";

#Connect
$conn->open($connect);

#Execute query
my $sql=$QUERY;

my $rs;
unless($rs=$conn->Execute($sql)){
  die("error executing the SQL statement");
}

$index=0;
$total_count=0;
#Get values
$myfldcnt = $rs->Fields->Count;
print("number $myfldcnt");

if ($myfldcnt == 8 ){
	while ( !$rs->EOF){
		$total_count++;

		$id = $rs->Fields(0)->value;

		$targname = $rs->Fields(1)->value;
		$targname =~ s/^ +//;	# strip leading & trailing spaces from the name
		$targname =~ s/ +$//;

		$asset = $rs->Fields(2)->value;
		$asset =~ s/^ +//;		# strip leading & trailing spaces from the name
		$asset =~ s/ +$//;

		$acqname = $rs->Fields(3)->value;
		$acqname =~ s/^ +//;	# strip leading & trailing spaces from the name
		$acqname =~ s/ +$//;

		$dealstatus = $rs->Fields(4)->value;
		$dealtype = $rs->Fields(5)->value;

		$sdate = $rs->Fields(6)->value;
		$levent = $rs->Fields(7)->value;

		$weef = sprintf("%d\t%s\t%s\t%s\t%s\t%s\t%s\t%s", $id, $targname, $asset, $acqname, $dealstatus, $dealtype, $sdate, $levent);
		print OUTFILE ( "$weef\n" );

		$rs->MoveNext;
	} 
	print("total records: $total_count\n");
} else {
	print ("number of fields is not equal to 5\n"); 
}

$rs->Close;
end:
$conn->Close;


close (OUTFILE);
