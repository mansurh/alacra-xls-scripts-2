set nocount on
set transaction isolation level read uncommitted

select CertificationState, RecordState, COUNT(*) as [Count]
FROM
	ipid_cici
GROUP BY CertificationState, RecordState
ORDER BY CertificationState, RecordState
