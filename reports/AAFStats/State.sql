set nocount on
set transaction isolation level read uncommitted

select SUBSTRING(State, 1, 20) AS State, COUNT(*) as [Count]
FROM
	ipid_cici
WHERE Country = 'US'
GROUP BY State
ORDER BY COUNT(*) desc