#!/bin/bash

XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

set -o pipefail
set -u

current=$(pwd)

mkdir -p logs
logfiledate=$(date +%Y-%m-%d-t%H%M)
logfile="${current}/logs/${logfiledate}-aod-invoice-mailing.log"

exec > >(tee -a "${logfile}")
exec 2> >(tee -a "${logfile}" >&2)

function handle_err {
    blat $(cygpath -w "${logfile}") -t "administrators@alacra.com" -s "AOD Invoice mailing"
	exit $1
}

date
echo

DBSERVER=`get_xls_registry_value xls server`
DBUSER=`get_xls_registry_value xls user`
DBPASSWORD=`get_xls_registry_value xls password`
SkValidServer=`get_xls_registry_value SkValidService2 server 2`


year=$(date +%Y)
# http://stackoverflow.com/questions/13168463/using-date-command-to-get-previous-current-and-next-month
month=$(date --date="$(date +%Y-%m-15) -1 month" +%m)

echo
echo "Get list of AOD accounts with purchases for ${month}/${year}"
read -r -d '' query <<EOF
set nocount on;
declare @fromDate datetime;
declare @toDate datetime;
set @fromDate = cast(${year} as char(4)) + '-' + cast(${month} as varchar(2)) + '-01'; 
set @toDate = dateadd(month,1,@fromDate);

select distinct a.id, a.name, a.billing_email 
from usage g join users u on g.userid = u.id 
join account a on a.id = u.account
where u.service = 79 and u.account != 7603
and g.access_time >= @fromDate 
and g.access_time < @toDate
and g.no_charge_flag is null
EOF
bcp "${query}" queryout "accounts.dat" -S ${DBSERVER} -U ${DBUSER} -P ${DBPASSWORD} -c -t "|" -CRAW
if [ $? -ne 0 ]; then echo "Error on queryout to accounts.dat, exiting"; handle_err 1; fi

echo
echo "Found accounts:"
cat accounts.dat

while IFS='|' read -r acct_id acct_name email; do
    echo
    url="http://${SkValidServer}:2222/?_msg=QUERYXLS&SQL=exec+dbo.aod_getAccountInvoiceUsageFromAccountId+${acct_id},+${month},+${year}"
    curl "${url}" -o account_info.xml
    if [ $? -ne 0 ]; then handle_err 1; fi
    msxsl account_info.xml $XLS/src/scripts/reports/client/aod/billing_users_invoice.xsl -o invoice.html
    if [ $? -ne 0 ]; then handle_err 1; fi
    htmltopdf invoice.html > response
    if [ $? -ne 0 ]; then handle_err 1; fi

    echo
    echo "Response from pdfconvert:"
    cat response
    
    pdf=$(head -n 1 response)
    if [ $? -ne 0 ]; then handle_err 1; fi

    pdf_name="${acct_id}_invoice_${month}${year}.pdf"
    echo
    echo "Renaming ${pdf} to ${pdf_name}"
    mv "${pdf}" "${pdf_name}"
    
    echo
    echo "Mailing ${pdf_name} to ${email} for account ${acct_id}."
    cat <<EOF > body.html
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
        <title>AOD Invoice - ${month}/${year}</title>
    </head>
    <body>
        <div id="logo">
            <img id="image" src="http://www.alacra.com/pubsite/OnDemand/images/logo.png"
                 alt="Company, Credit, Deal, Investmenet &amp; Market Research"/>
        </div>  
        <hr/>
        <p>Your Alacra On Demand invoice for ${month}/${year} is available for viewing. <br/>
            It is attached and also avaible for viewing online, by logging into your account at <a href="ondemand.alacra.com">ondemand.alacra.com</a>.
        </p>
        <p>Thank you for your business,</p>
        <p>Alacra On Demand team</p>
    </body>
</html>
EOF
    echo blat body.html -to ${email} -subject "AOD Invoice - ${month}/${year}" -attach ${pdf_name} -html
    blat body.html -to ${email} -subject "AOD Invoice - ${month}/${year}" -attach ${pdf_name} -html
    if [ $? -ne 0 ]; then handle_err 1; fi
done < accounts.dat

blat - -body "Sent invoices to the following accounts (attached)" -to "matt.strauss@alacra.com" -subject "AOD Invoice - ${month}/${year}" -attach accounts.dat
