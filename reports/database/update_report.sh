XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# Parse command line
# Parse the command line arguments.
#	1 = server

DBASERVER=$1
if [ $#  -lt 1 ]
then
  DBAUSER=`get_xls_registry_value dba2 Server`
fi

# Save the runstring parameters in local variables
DBAUSER=`get_xls_registry_value dba2 User`
DBAPASSWORD=`get_xls_registry_value dba2 Password`

emailgrp="michael.angle@opus.com,administrators@alacra.com,albert.tamayev@opus.com,Matthew.Strauss@Opus.Com"
DatabaseReportList=DatabaseReportList`date +%y%m%d`
updatereport=update_report`date +%y%m%d`
run () {
  rm -f ${DatabaseReportList}*
  rm -f ${updatereport}*

  bcp "exec UpdateReport" queryout ${DatabaseReportList}.xls.tmp /U${DBAUSER} /P${DBAPASSWORD} /S${DBASERVER} /c
  if [ $? -ne 0 ]; then echo "error executing UpdateReport"; exit 1; fi;

  # Since we moved the SQL statement to a stored proc that produces 8 columns
  # we need to reformat the headers.  The First five lines, contain header info
  # and we are stripping off the Undersores line, replacing it with our own
  echo "Application              Server       Dbname           Component                Last update  Update freq  Update sched Overdue By   Developer " > ${updatereport}.txt
  echo "------------------------ ------------ ---------------- ------------------------ ------------ ------------ ------------ ------------ ------------" >> ${updatereport}.txt
  echo
  cat ${DatabaseReportList}.xls.tmp |cut -f 1-9  |  sed 's/\t/ /g' >> ${updatereport}.txt

  #create output for Excel file
  echo -e "Application\tServer\tDbname\tComponent\tLast update\tUpdate freq\tUpdate sched\tOverdue By\tDeveloper" > ${DatabaseReportList}.xls
  cat ${DatabaseReportList}.xls.tmp  >> ${DatabaseReportList}.xls

  # Get a database report for acm collections as well
  $XLS/src/scripts/reports/database/update_report_acm.sh ${updatereport}_acm.txt ${DatabaseReportList}ACM.xls
  if [ $? -ne 0 ]
  then
     cat "Error getting acm report" >> ${updatereport}.txt 
     touch ${DatabaseReportList}ACM.xls 
  else
     cat ${updatereport}_acm.txt  >> ${updatereport}.txt 
  fi

  blat ${updatereport}.txt -attach ${DatabaseReportList}.xls -attach ${DatabaseReportList}ACM.xls -t "${emailgrp}"  -s "Database Update Report"
}

logfile=${updatereport}.log
(run) > ${logfile} 2>&1
if [ $? -ne 0 ]
then
  blat ${logfile} -t administrators@alacra.com -s "Database Update report failed"
  exit 1
fi

exit 0
