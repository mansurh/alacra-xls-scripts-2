set nocount on
set transaction isolation level read uncommitted

select CASE WHEN Register_Number IS NULL THEN 'No' ELSE 'Yes' END AS Has_Register_Number, COUNT(*) as [Count]
FROM
	ipid_gei
GROUP BY CASE WHEN Register_Number IS NULL THEN 'No' ELSE 'Yes' END 
ORDER BY COUNT(*) desc
