PRINT "Datamonitor Daily Usage Report"
PRINT ""
PRINT ""
select CONVERT(varchar(16),GETDATE(),107) "Today's Date"
PRINT ""
PRINT ""


PRINT "Subscribers expiring in the next 30 days"
PRINT "========================================"
select
	convert (varchar(12),v.name) "Service",
	convert (varchar(24),COALESCE(p.company,a.name)) "Company",
	convert (varchar(16),p.last_name) "Last Name",
	convert (varchar(12),p.first_name) "First Name",
	convert (varchar(12),p.login) "User-ID",
	convert (varchar(12),p.terminate,107) "Expiration",
	COALESCE (convert (varchar(3),sp.abbreviation),"") "ST",
	COALESCE (convert (varchar(8),c.name),"") "Country"
from
	users p, account a, service v, state_prov sp, country c
where
	p.terminate >= GETDATE()
	and
	p.terminate <= DATEADD(day,30,GETDATE())
	and
	p.demo_flag is null
	and
	p.account = a.id
	and
	p.service = v.id
	and
	a.state_prov *= sp.id
	and
	a.country *= c.id
	and
	p.service = 51
order by
	convert (varchar(12), v.name), convert (varchar(24),COALESCE(p.company,a.name)), p.last_name
	
PRINT ""
PRINT ""

PRINT "Demo User IDs expiring in the next 7 days"
PRINT "========================================="
select
	convert (varchar(12),v.name) "Service",
	convert (varchar(24),COALESCE(p.company,a.name)) "Company",
	convert (varchar(16),p.last_name) "Last Name",
	convert (varchar(12),p.first_name) "First Name",
	convert (varchar(12),p.login) "User-ID",
	convert (varchar(12),p.terminate,107) "Expiration"
from
	users p, account a, service v
where
	p.terminate >= GETDATE()
	and
	p.terminate <= DATEADD(day,7,GETDATE())
	and
	p.demo_flag is not null
	and
	p.account *= a.id
	and
	p.service = v.id
	and
	p.service = 51
order by 
	convert (varchar(12), v.name), convert (varchar(24),COALESCE(p.company,a.name)), p.last_name

PRINT ""
PRINT ""

PRINT "Subscribers who have never logged in"
PRINT "===================================="
select
	convert (varchar(12),v.name) "Service",
	convert (varchar(24),COALESCE(p.company,a.name)) "Company",
	convert (varchar(16),p.last_name) "Last Name",
	convert (varchar(12),p.first_name) "First Name",
	convert (varchar(12),p.login) "User-ID",
	convert (varchar(12),p.terminate,107) "Expiration"
from
	users p, account a, service v
where
	p.demo_flag is null
	and
	p.acceptance is null
	and
	p.terminate >= GETDATE()
	and
	p.account *= a.id
	and
	p.service = v.id
	and
	p.service = 51
order by 
	convert (varchar(12), v.name), convert (varchar(24),COALESCE(p.company,a.name)), p.last_name
	
PRINT ""
PRINT ""

PRINT "User Downloads in the past 7 days"
PRINT "================================================================"
select 
	convert (varchar(24),COALESCE(p.company,a.name)) "Company",
	convert (varchar(16),p.last_name) "Last Name",
	convert (varchar(12),p.first_name) "First Name",
	convert (varchar(12),p.login) "User-ID",
	convert (varchar(16),i.name) "Content Provider", 
	convert (varchar(4), count(u.list_price)) "Count", 
	convert (varchar(10),sum(u.list_price)) "Total $",
	substring (convert (varchar(8),max(u.access_time),112),5,4) "Last Access"
from 
	usage u, users p, ip i, account a
where
	u.access_time >= DATEADD(day,-7,GETDATE())
and
	u.userid = p.id
and
	u.ip = i.id
and
	p.demo_flag is null
and
	p.account *= a.id
	and
	p.service = 51
group by 
	convert (varchar(24),COALESCE(p.company,a.name)),p.last_name, p.first_name, p.login, i.name
order by
	convert (varchar(24),COALESCE(p.company,a.name)),p.last_name, p.first_name, p.login, i.name

go
