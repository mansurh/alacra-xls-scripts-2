# SPIDERDB_RECHECK.PL - Tests URLs fram a file
# Takes a file call files.txt in using something like 
#	ls -1 <pattern like 2*.dat>  > files.txt
#

use Win32::OLE;
use OLE;
use LWP::UserAgent;

$ua = new LWP::UserAgent;

my $conn=CreateObject OLE "ADODB.Connection" || die "CreateObject: $!"; 
my $connect="DRIVER={SQL SERVER};PWD=spiderdb;UID=spiderdb;SERVER=data5;DATABASE=spiderdb"; 
$conn->open($connect);

$mode = $ARGV[0];
$sublist = 0;
$skipthis = 0;
open (urlfile, "f:\\users\\default\\spider\\spiderErr.log");
$afile = <urlfile>;
while ($afile ne ""){
	chomp($afile);
#	print "$afile\n";
	@piece = split(/\|/, $afile);
	$siteid = $piece[0];
	$xlsid = $piece[1];
	$desc = $piece[2];
	$url = $piece[3];

#	print "$url\n";
	#Switch sublist if the right headers come out
	if ($afile eq "SITE_ID|XLS_ID|NAME|VALUE"){
		$skipthis = 1;
		print "SITE_ID|XLS_ID|NAME|VALUE\n";
	}
	if ($afile eq "NO HOST FOUND"){
		$sublist = 1;
		$skipthis = 1;
		print "======================NO HOST FOUND============================\n";
		print "======================NO HOST FOUND============================\n";
		print "======================NO HOST FOUND============================\n";
	}
	if ($afile eq "SERVER ERROR"){
		$sublist = 2;
		$skipthis = 1;
		print "======================SERVER ERROR============================\n";
		print "======================SERVER ERROR============================\n";
		print "======================SERVER ERROR============================\n";

	}	
	if ($afile eq "FILE NOT FOUND"){
		$sublist = 3;
		$skipthis = 1;
		print "======================FILE NOT FOUND============================\n";
		print "======================FILE NOT FOUND============================\n";
		print "======================FILE NOT FOUND============================\n";
	}
	if ($afile eq "FILE FORBIDDEN"){
		$sublist = 4;
		$skipthis = 1;
		print "======================FILE FORBIDDEN============================\n";
		print "======================FILE FORBIDDEN============================\n";
		print "======================FILE FORBIDDEN============================\n";
	}
	if ($mode eq "xlsid"){
		if (length("xlsid") == 0){
			$skipthis = 1;
		}
	}
	if ($skipthis != 1){	
	
		my $req = new HTTP::Request 'GET', $url;
		$req->content_type('application/x-www-form-urlencoded');
		my $res = $ua->request($req);
	
#		print $res->as_string;
		@htmlres = split(/\n/, $res->as_string);
		$res1 = lc($htmlres[0]);
		$pos = index($res1, "error");
		if ($pos > 0){
			if ($mode eq "xlsid"){
				if (length($xlsid) > 0){
					print "	${siteid}|${xlsid}|${desc}|${url}|";
					print "$htmlres[0]\n";
				}
			}else{
					print "	${siteid}|${xlsid}|${desc}|${url}|";
					print "$htmlres[0]\n";
			}
		}
#		$sql = " Select * from spiderdb..sites where site_id = ";
#		$sql .= $afile;	
#		$rs = $conn->Execute(${sql}) || die "Execute Error on (${sql}) : $!";
#		$siteid = $rs->Fields('site_id')->value;
#		if ( $rs->EOF ){
#			print " problem with ${afile} \n";
#		}
#		$rs->Close;

	}
	$skipthis = 0;	
	$afile = <urlfile>;

}
