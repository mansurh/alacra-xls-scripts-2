
use strict;
use IO::Socket::INET;

# Input params
my $port = 4444;  #4444 is gis2 service, which routes to 4446 gis process 
my $host = @ARGV[0];
my $query = @ARGV[1];

# Connect to server
my $s = IO::Socket::INET->new(	PeerAddr => $host,
				PeerPort => $port,
				Proto => "tcp",
				Type => SOCK_STREAM)
  or die "Couldn't connect to $host:$port : $!\n";

# Send query
print $s "$query\n";

print "";

# Recv return
my $data;


while( <$s> ) { $data .= $_; }

#$s->read($data,100000);

#recv($s,$data,100000,0);


# Close connection
close $s;


print "<?xml version='1.0'  encoding='ISO-8859-1'?>";
print $data;

