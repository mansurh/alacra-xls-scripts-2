set nocount on
set transaction isolation level read uncommitted 

/* The Date */
select 'Period ending ' + CONVERT (varchar(32),GETDATE(),107), NULL, 1 'sort'

UNION

/* Number of Companies with Quotes */
select 'Companies with Quotes', count(distinct ep.property_value), 2 'sort'
from 
event e,
event_type et,
event_property ep,
property_spec ps,
snapshot1 s
where
e.event_type = et.id
and
et.name='AnalystQuote'
and
e.date >= DATEADD (day,-30,GETDATE())
and
e.id = ep.event_id
and
ep.property_id = ps.property_id
and
ps.event_type = et.id
and
ps.xpath = 'CompanyId'
and
ep.property_value = s.xlsid

UNION 

/* Number of Firms with Quotes */
select 'Research Firms with Quotes', count(distinct ep2.property_value), 3 'sort'
from 
event e,
event_type et,
event_property ep,
property_spec ps,
event_property ep2,
property_spec ps2
where
e.event_type = et.id
and
et.name='AnalystQuote'
and
e.date >= DATEADD (day,-30,GETDATE())
and
e.id = ep.event_id
and
ep.property_id = ps.property_id
and
ps.event_type = et.id
and
ps.xpath = 'CompanyId'
and
e.id = ep2.event_id
and
ep2.property_id = ps2.property_id
and
ps2.event_type = et.id
and
ps2.xpath = 'Institution'

UNION

/* Number of Analysts with Quotes */
select 'Analysts with Quotes', count(distinct ep2.property_value), 4 'sort'
from 
event e,
event_type et,
event_property ep,
property_spec ps,
event_property ep2,
property_spec ps2
where
e.event_type = et.id
and
et.name='AnalystQuote'
and
e.date >= DATEADD (day,-30,GETDATE())
and
e.id = ep.event_id
and
ep.property_id = ps.property_id
and
ps.event_type = et.id
and
ps.xpath = 'CompanyId'
and
e.id = ep2.event_id
and
ep2.property_id = ps2.property_id
and
ps2.event_type = et.id
and
ps2.xpath = 'Analyst'

UNION

/* Number of Quotes by Traditional Analysts */
select 'Traditional Analyst Quotes', count(distinct e.id), 5 'sort'
from 
event e,
event_type et,
event_property ep2,
property_spec ps2,
event_property ep3,
property_spec ps3,
analyst_firm af
where
e.event_type = et.id
and
et.name='AnalystQuote'
and
e.date >= DATEADD (day,-30,GETDATE())
and
e.id = ep2.event_id
and
ep2.property_id = ps2.property_id
and
ps2.event_type = et.id
and
ps2.xpath = 'AnalystId'
and
e.id = ep3.event_id
and
ep3.property_id = ps3.property_id
and
ps3.event_type = et.id
and
ps3.xpath = 'InstitutionId'
and 
ep2.property_value = CONVERT (varchar(255),af.analyst_id)
and
ep3.property_value = CONVERT (varchar(255),af.firm_id)
and
e.date >= af.date_started and (e.date <= af.date_ended or af.date_ended is NULL)
and
af.analyst_firm_id not in (select analyst_firm_id from analyst_firm_blog)

UNION

/* Number of Quotes by Alternative Media */
select 'Blogger Quotes', count(distinct e.id), 6 'sort'
from 
event e,
event_type et,
event_property ep2,
property_spec ps2,
event_property ep3,
property_spec ps3,
analyst_firm af
where
e.event_type = et.id
and
et.name='AnalystQuote'
and
e.date >= DATEADD (day,-30,GETDATE())
and
e.id = ep2.event_id
and
ep2.property_id = ps2.property_id
and
ps2.event_type = et.id
and
ps2.xpath = 'AnalystId'
and
e.id = ep3.event_id
and
ep3.property_id = ps3.property_id
and
ps3.event_type = et.id
and
ps3.xpath = 'InstitutionId'
and 
ep2.property_value = CONVERT (varchar(255),af.analyst_id)
and
ep3.property_value = CONVERT (varchar(255),af.firm_id)
and
e.date >= af.date_started and (e.date <= af.date_ended or af.date_ended is NULL)
and
af.analyst_firm_id in (select analyst_firm_id from analyst_firm_blog)

UNION

/* Number of Quotes */
select 'Total Quotes', count(distinct e.id), 7 'sort'
from 
event e,
event_type et
where
e.event_type = et.id
and
et.name='AnalystQuote'
and
e.date >= DATEADD (day,-30,GETDATE())

UNION

/* Number of Stories with Quotes */
select 'Total Stories with Quotes', count(distinct e.doc_id), 8 'sort'
from 
event e,
event_type et
where
e.event_type = et.id
and
et.name='AnalystQuote'
and
e.date >= DATEADD (day,-30,GETDATE())

UNION

/* Feeds Reviewed */
select 'Feeds Reviewed', count(*), 9 'sort'
from feeds

UNION

/* Total Stories Reviewed */
select 'Total Stories Reviewed', count(distinct url), 10 'sort'
from 
weblogs 
where 
added >= DATEADD (day,-30,GETDATE())

order by sort asc


