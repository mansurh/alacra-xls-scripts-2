set nocount on
set transaction isolation level read uncommitted

/* Statistics by Research Firm */
select ep2.property_value 'Firm', count(distinct ep.property_value) 'Companies', count(*) 'Quotes', ep3.property_value 'FirmID'
from 
event e,
event_type et,
event_property ep,
property_spec ps,
event_property ep2,
property_spec ps2,
event_property ep3,
property_spec ps3
where
e.event_type = et.id
and
et.name='AnalystQuote'
and
e.date >= DATEADD(day,-30,GETDATE())
and
e.id = ep.event_id
and
ep.property_id = ps.property_id
and
ps.event_type = et.id
and
ps.xpath = 'CompanyId'
and
e.id = ep2.event_id
and
ep2.property_id = ps2.property_id
and
ps2.event_type = et.id
and
ps2.xpath = 'Institution'
and
e.id = ep3.event_id
and
ep3.property_id = ps3.property_id
and
ps3.event_type = et.id
and
ps3.xpath = 'InstitutionID'
group by ep3.property_value, ep2.property_value
order by count(distinct ep.property_value) desc