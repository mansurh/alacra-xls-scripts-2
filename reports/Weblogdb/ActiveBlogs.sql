set nocount on
set transaction isolation level read uncommitted

/* Active Blogs */
select distinct c.analyst_firm_id, a.analyst_id, a.first_name, a.last_name, 
b.id, b.name, d.feed_url, d.blog_url, 
CASE WHEN d.multiple_authors=1 THEN 'Yes' ELSE 'No' END
from analysts a, firms b, analyst_firm c, analyst_firm_blog d
where a.analyst_id=c.analyst_id and b.id=c.firm_id
and c.analyst_firm_id=d.analyst_firm_id and c.date_ended is null
and d.feed_id in
(select id from feed_candidates)
order by a.last_name, a.first_name, b.name