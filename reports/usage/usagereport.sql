set nocount on
set tran isolation level read uncommitted

PRINT "Data Downlink Daily Usage Report"
PRINT ""
PRINT ""
select CONVERT(varchar(16),GETDATE(),107) "Today's Date"
PRINT ""
PRINT ""

PRINT "Subscriber Downloads by User, Content Provider in the last 7 days"
PRINT "================================================================="
select 
	convert (varchar(24),COALESCE (p.company, a.name)),
	convert (varchar(16),p.last_name) "Last Name",
	convert (varchar(16),p.first_name) "First Name",
	convert (varchar(12),p.login) "User-ID",
	convert (varchar(50),i.name) "Content Provider", 
	convert (varchar(4), count(u.price)) "Count", 
	convert (varchar(10),sum(u.price)) "Total $",
	convert (varchar(12),max(u.access_time),107) "Last",
	convert (varchar(12),max(s.prepbookid)) "Alacra Book ID"
from 
	usage u join users p on u.userid = p.id
	join ip i on u.ip = i.id 
	full join account a on p.account = a.id
	full join shoppingcart s on u.shoppingcartid = s.shoppingcartid
where
	u.access_time >= DATEADD(day,-7,CONVERT(varchar(9),GETDATE(),1))
and
	u.access_time < CONVERT(varchar(9),GETDATE(),1)
and
	u.access_time >= p.commence
and
	p.demo_flag is null
and
	u.no_charge_flag is NULL
group by 
	convert (varchar(24),COALESCE (p.company, a.name)),p.last_name, p.first_name, p.login, i.name
order by
	convert (varchar(24),COALESCE (p.company, a.name)) asc

PRINT "Subscriber Downloads by User, Content Provider in the last 24 hours"
PRINT "================================================================="
select 
	convert (varchar(24),COALESCE (p.company, a.name)),
	convert (varchar(16),p.last_name) "Last Name",
	convert (varchar(16),p.first_name) "First Name",
	convert (varchar(12),p.login) "User-ID",
	convert (varchar(50),i.name) "Content Provider", 
	convert (varchar(4), count(u.price)) "Count", 
	convert (varchar(10),sum(u.price)) "Total $",
	convert (varchar(12),max(u.access_time),107) "Last",
	convert (varchar(12),max(s.prepbookid)) "Alacra Book ID"
from 
	usage u join users p on u.userid = p.id
	join ip i on u.ip = i.id 
	full join account a on p.account = a.id
	full join shoppingcart s on u.shoppingcartid = s.shoppingcartid
where
	u.access_time >= DATEADD(day,-1,CONVERT(varchar(9),GETDATE(),1))
and
	u.access_time < CONVERT(varchar(9),GETDATE(),1)
and
	u.access_time >= p.commence
and
	p.demo_flag is null
and
	u.no_charge_flag is NULL
group by 
	convert (varchar(24),COALESCE (p.company, a.name)),p.last_name, p.first_name, p.login, i.name
order by
	convert (varchar(24),COALESCE (p.company, a.name)) asc

PRINT ""
PRINT "Billable Downloads in the last 24 hours"
PRINT "======================================="
select 
	convert (varchar(10),sum(u.price)) "Total $"
from 
	usage u join users p on u.userid = p.id
	join ip i on u.ip = i.id 
	full join account a on p.account = a.id
where
	u.access_time >= DATEADD(day,-1,CONVERT(varchar(9),GETDATE(),1))
and
	u.access_time < CONVERT(varchar(9),GETDATE(),1)
and
	u.access_time >= p.commence
and
	p.demo_flag is null
and
	u.no_charge_flag is NULL
go

PRINT ""
PRINT "Production Prepbooks Created in the last 24 hours"
PRINT "================================================="
select 
	convert (varchar(24),COALESCE(u.company,a.name)) "Company", convert(varchar(24),u.login) "User", count(*) "Count"
from 
	prepbook p join users u on p.userid = u.id
	join account a on u.account = a.id
where 
	p.pbcreatetime > DATEADD(day,-1,GETDATE())
	and
	u.demo_flag is null
group by
	convert (varchar(24),COALESCE(u.company,a.name)), convert(varchar(24),u.login)
order by
	convert (varchar(24),COALESCE(u.company,a.name)), convert(varchar(24),u.login) asc


PRINT ""
PRINT "Production Prepbooks Completed in the last 24 hours"
PRINT "==================================================="
select 
	convert (varchar(24),COALESCE(u.company,a.name)) "Company", convert(varchar(24),u.login) "User", count(*) "Count"
from 
	pbusage p join users u on p.userid = u.id
	join account a on u.account = a.id
where 
	p.pbendtime > DATEADD(day,-1,GETDATE())
	and
	u.demo_flag is null
--	and
--	p.pbstatus in (3,5)
group by
	convert (varchar(24),COALESCE(u.company,a.name)), convert(varchar(24),u.login)
order by
	convert (varchar(24),COALESCE(u.company,a.name)), convert(varchar(24),u.login) asc


PRINT ""
PRINT "Production Word Books Completed in the last 24 hours"
PRINT "==================================================="
select 
	convert (varchar(24),COALESCE(u.company,a.name)) "Company", convert(varchar(24),u.login) "User", count(*) "Count"
from 
	usage p join users u on p.userid = u.id
	join account a on u.account = a.id
where 
	p.access_time > DATEADD(day,-1,GETDATE())
	and
	p.description like 'Word Book%'
group by
	convert (varchar(24),COALESCE(u.company,a.name)), convert(varchar(24),u.login)
order by
	convert (varchar(24),COALESCE(u.company,a.name)), convert(varchar(24),u.login) asc


PRINT ""
PRINT "Production Word Books Completed in the last 24 hours: details"
PRINT "==================================================="
select 
	convert (varchar(24),COALESCE(u.company,a.name)) "Company", convert(varchar(24),u.login) "User", p.description "Description"
from 
	usage p join users u on p.userid = u.id
	join account a on u.account = a.id
where 
	p.access_time > DATEADD(day,-1,GETDATE())
	and
	p.description like 'Word Book%'
order by
	convert (varchar(24),COALESCE(u.company,a.name)), convert(varchar(24),u.login) asc



PRINT ""
PRINT "Pay-Per-View Usage in Databases that are Subscription Only"
PRINT "==========================================================="
select p.company, p.login, u.description, u.access_time, u.price  
from usage u join users p on u.userid = p.id
where u.ip in (109,121,111) and u.price <> 0
	and
	p.demo_flag is null
	and
	u.access_time >= DATEADD(hour,-24,GETDATE())
order by  
	u.access_time desc
