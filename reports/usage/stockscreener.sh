# Shell script to get the stockscreener usage for the previous day

# Build the name of the log file
date=`date +%d%b`
filename="access2.${date}-12AM"

# Get the number of hits in a file
echo "Stockscreener views on Data Downlink servers for 24 hour period ending ${date}-12AM" > hoovers.mail
echo "Views = \c" >> hoovers.mail
grep stockscreener.exe ${filename} | wc -l >> hoovers.mail

# Mail the results
c:/usr/local/bin/blat hoovers.mail -t sstraffic@hoovers.com,sgoldstein@xls.com,mangle@xls.com -s "Stockscreener DDC Usage"
