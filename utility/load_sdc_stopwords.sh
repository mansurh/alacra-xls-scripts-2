XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/check_arguments.fn
. $XLSUTILS/check_bcp_errors.fn
. $XLSUTILS/check_return_code.fn

check_arguments $# 4 "Usage: load_sdc_stopwords.sh server user pass filepath" 1

server=$1
user=$2
pass=$3
filepath=$4

loadfile=stopwords.txt.`date +%d%m%H`
cat ${filepath} > ${loadfile}
rm -f ${filepath}

isql -S ${server} -U ${user} -P ${pass} -b < $XLS/schema/sdc/StopWordNames/create.sql
check_return_code $? "Error creating StopWordNames table. Aborting" $?
isql -S ${server} -U ${user} -P ${pass} < $XLS/schema/sdc/StopWordNames/create_index.sql

cmd="bcp StopWordNames in ${loadfile} -S ${server} -U ${user} -P ${pass} -c -o StopWordNames.errout"
#echo $cmd
$cmd
check_return_code $? "Failed to bcp StopWordNames table. Aborting" 1
check_bcp_errors StopWordNames.errout 1 "Failed to bcp StopWordNames table. Aborting" 1

rm -f ${loadfile}

isql -S ${server} -U ${user} -P ${pass} -b < $XLS/schema/sdc/targetFinancials/create.sql
check_return_code $? "Error creating targetFinancials table. Aborting" $?
isql -S ${server} -U ${user} -P ${pass} < $XLS/schema/sdc/targetFinancials/create_index.sql

isql -S ${server} -U ${user} -P ${pass} -b < $XLS/schema/sdc/transactionStatistics/create.sql
check_return_code $? "Error creating transactionStatistics table. Aborting" $?
isql -S ${server} -U ${user} -P ${pass} < $XLS/schema/sdc/transactionStatistics/create_index.sql

#isql -S ${server} -U ${user} -P ${pass} -b < $XLS/schema/sdc/DealCounts/create.sql
#check_return_code $? "Error creating DealCounts table. Aborting" $?
#isql -S ${server} -U ${user} -P ${pass} < $XLS/schema/sdc/DealCounts/create_index.sql

exit 0