XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/check_arguments.fn
. $XLSUTILS/check_bcp_errors.fn

check_arguments $# 4 "Usage: load_ftinfo.sh server user pass filepath" 1

server=$1
user=$2
pass=$3
filepath=$4

main() {
  date
  dbname=`isql  -S ${server} -U ${user} -P ${pass} -Q"set nocount on; select db_name()" -h-1`
  if [[ $dbname == acm* ]];
  then
    echo "$dbane db, no need to update ftinfo, exiting..."
    return 0
  fi

  cat ${filepath} > ${loadfile}

  #try quering the ftinfo table (to check if it exists)
  isql -S ${server} -U ${user} -P ${pass} -Q " select top 0 * from ftinfo " -b -n -h-1
  if [ $? != 0 ]
  then
	echo "Ftinfo table doesn't exist, try creating it"
	isql -S ${server} -U ${user} -P ${pass} -b < $XLS/schema/gentext/ftinfo/create.sql
	if [ $? -ne 0 ]; then return 1; fi

	isql -S ${server} -U ${user} -P ${pass} < $XLS/schema/gentext/ftinfo/create_index.sql
  fi

  echo "drop ftinfo_tmp table (in case it exists)"
  isql -S ${server} -U ${user} -P ${pass} -Q " drop table ftinfo_tmp "

  isql -S ${server} -U ${user} -P ${pass} -Q " select top 0 doc_id, name, value into ftinfo_tmp from ftinfo " -b
  if [ $? -ne 0 ]; then return 1; fi

  cmd="bcp ftinfo_tmp in ${loadfile} -S ${server} -U ${user} -P ${pass} -c -o ftinfo.errout"
  echo $cmd
  date
  $cmd
  if [ $? -ne 0 ]; then return 1; fi

  echo "de-duping existing ftinfo data..." 
  date
  isql -S ${server} -U ${user} -P ${pass} -Q " delete from i from ftinfo i, ftinfo_tmp t where i.doc_id = t.doc_id " -b 
  if [ $? -ne 0 ]; then return 1; fi

  echo "inserting new ftinfo data..."
  date
  isql -S ${server} -U ${user} -P ${pass} -Q " insert into ftinfo (doc_id, name, value) select * from ftinfo_tmp " -b 
  if [ $? -ne 0 ]; then return 1; fi

  date
  return 0
}

loadfile=ftinfo.txt.`date +%d%m%H`
logfile=ftinfo`date +%d%m%H`.log
main >> ${logfile} 2>&1
if [ $? -ne 0 ]
then
	isql -S ${server} -U ${user} -P ${pass} -Q " drop table ftinfo_tmp "
	echo "try rerunning load_ftinfo.sh with file ${loadfile}" >> ${logfile}
	blat ${logfile} -t administrators@alacra.com -s "Error: failed to load ftino for ${user} on ${server}"
else
	isql -S ${server} -U ${user} -P ${pass} -Q " drop table ftinfo_tmp "
	rm -f ${loadfile}
	rm -f ${logfile}
fi
rm -f ${filepath}
exit 0