XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn
if [ $# -lt 2 ]
then
   echo "usage: $0 sourcemachine idxname [backup_dir]"
   exit 1
fi

source=$1
idxname=$2

sserver=`get_xls_registry_value sitemapdb Server`
suser=`get_xls_registry_value sitemapdb User`
spass=`get_xls_registry_value sitemapdb Password`

currentloc=`reginvidx get $idxname`

scratch=${currentloc%/*}
indexdir=${scratch}

typeset -i startearly=0
if [ $# -gt 2 ]
then
	startearly=1
	targetdocdir=$3
else
	targetdocdir=${indexdir}/backup
fi

mkdir -p ${targetdocdir}
rm -r -f ${targetdocdir}/*

newindexdir=${indexdir}/new

# Change to the index directory
cd ${indexdir}
if [ $? != 0 ]
then
    echo "Error changing to index directory, Exiting"
    exit 1
fi

mkdir -p ${newindexdir}
cd ${newindexdir}
if [ $? != 0 ]
then
    echo "Error changing to new index directory, Exiting"
    exit 1
fi
#remove anything present in "new" directory
rm -r -f *

if [ $startearly -eq 1 ]
then
	# Report start of process into DBA database
	$XLS/src/scripts/loading/dba/startupdate.sh ${idxname} ${COMPUTERNAME} ${idxname}
	if [ $? != 0 ]
	then
		echo "${shname}: Error from startupdate.sh, Exiting"
		blat ${logfilename} -t "administrators@xls.com" -s "Investext Update Failed"
		exit 1
	fi

	$XLS/src/scripts/utility/moveInvFileSet.sh ${idxname} ${indexdir} ${targetdocdir}
	if [ $? != 0 ]
	then
		echo "${shname}: Error moving old index files, exiting"
		exit 1
	fi
fi

#echo "user filepickup filepickup" > ftp_copy.cmd
#echo "cd /invindex/${idxname}" >> ftp_copy.cmd
#echo "binary"  >> ftp_copy.cmd
#echo "mget ${idxname}* " >> ftp_copy.cmd
#echo "quit" >> ftp_copy.cmd

#ftp -i -n -s:ftp_copy.cmd ${source}
ncftpget -u filepickup -p filepickup -V -Z -t 60 ${source} . "/invindex/${idxname}/${idxname}*"
if [ $? -ne 0 ]
then
	echo "error in ftp, exiting ..."
	exit 1
fi

if [ ! -s ${idxname}_sizes.txt ]
then
	echo "did not get ${idxname}_sizes.txt, can't do size comparisons, exiting..."
	exit 1
fi

unix2dos ${idxname}_sizes.txt 
mv ${idxname}_sizes.txt ${indexdir}
rm -f ${idxname}_new_sizes.txt
ls -l ${idxname}* |awk '{ print $5, $9 }' > ${idxname}_new_sizes.txt

unix2dos ${idxname}_new_sizes.txt
diff ${newindexdir}/${idxname}_new_sizes.txt ${indexdir}/${idxname}_sizes.txt
if [ $? != 0 ]
then
	echo "error in size comparisons, exiting ..."
	exit 1
fi

if [ $startearly -eq 0 ]
then
	# Report start of process into DBA database
	$XLS/src/scripts/loading/dba/startupdate.sh ${idxname} ${COMPUTERNAME} ${idxname}
	if [ $? != 0 ]
	then
		echo "${shname}: Error from startupdate.sh, Exiting"
		blat ${logfilename} -t "administrators@xls.com" -s "Investext Update Failed"
		exit 1
	fi

	$XLS/src/scripts/utility/moveInvFileSet.sh ${idxname} ${indexdir} ${targetdocdir}
	if [ $? != 0 ]
	then
		echo "${shname}: Error moving old index files, exiting"
		exit 1
	fi
fi

$XLS/src/scripts/utility/moveInvFileSet.sh ${idxname} ${newindexdir} ${indexdir} 
if [ $? != 0 ]
then
	echo "${shname}: Error moving new index files, exiting"
	exit 1
fi

cd ${indexdir}
$XLS/src/scripts/sitemaps/renameSitemaps.sh ${source}
if [ $? -ne 0 ]
then
	echo "error in renaming sitemaps, exiting ..."
	exit 1
fi

#update sitemapdb database with new sitemap files
isql /S${sserver} /U${suser} /P${spass} /b /Q " exec renameSitemaps '${COMPUTERNAME}', '${source}', '${idxname}' "
if [ $? -ne 0 ]
then
	echo "error in isql renaming sitemaps, exiting ..."
	exit 1
fi

$XLS/src/scripts/loading/dba/endupdate.sh ${idxname} ${COMPUTERNAME} ${idxname}
exit 0