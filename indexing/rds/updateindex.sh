TITLEBAR="B&I Index Update"

# Parse the command line arguments.
#	1 = [Date]

# Build the date
updatedate=`date +%"B %d, %Y"`
if [ $# -eq 1 ]
then
	updatedate=$1
fi

dataserver=`hostname`

# where is the index located?
currentloc=`reginvidx get nn`

scratch=${currentloc%/*}
indexdir=${scratch}
cd ${indexdir}
if [ $? -ne 0 ]
then
	echo "Index directory not found, exiting"
	exit 1
fi

# Make sure the index files exist in the current directory
if [ ! -e bniindex.idx ]
then
	echo "Index file not found, exiting"
	exit 1
fi
if [ ! -e bniindex.doc ]
then
	echo "Document file not found, exiting"
	exit 1
fi

# Mark the database as updating
$XLS/src/scripts/loading/dba/startupdate.sh rds ${dataserver} rds
if [ $? -ne 0 ]
then
	echo "error in startupdate.sh rds ${dataserver} rds"
	exit 1
fi

# Now update the index
newrdsindex.exe -d bni -l "$updatedate" -t 2000000 -m 20000000
if [ $? -ne 0 ]
then
	echo "Error updating news indices, Exiting"
	exit 1
fi

# Mark the database update as complete
$XLS/src/scripts/loading/dba/endupdate.sh rds ${dataserver} rds
