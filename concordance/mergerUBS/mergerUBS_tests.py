# $python bulk_merge_tests.py
import unittest
import inspect
from mergerUBS import getMapping, Entity

class BulkMergeTests(unittest.TestCase):
	def setUp(self):
		print("")
		self.logPoint()
		self.entity = Entity()
		self.entity.mappings = [{'basefeed': False, 'dead': False, 'source': 9964, 'sourcekey': u'1004664', 'xlsid': 5137285}, 
					{'basefeed': False, 'dead': False, 'source': 11007, 'sourcekey': u'US*190521527280', 'xlsid': 5137285},
					{'basefeed': True, 'dead': False, 'source': 11052, 'sourcekey': u'BD000000B62W', 'xlsid': 5137285}, 
					{'basefeed': False, 'dead': False, 'source': 111113, 'sourcekey': u'33647Z', 'xlsid': 5137285}, 
					{'basefeed': False, 'dead': False, 'source': 111136, 'sourcekey': u'41553', 'xlsid': 5137285}]
		
	def test_get_mapping(self):
		self.logPoint()
		self.assertEqual(getMapping(self.entity, 9964), '1004664')
		self.assertNotEqual(getMapping(self.entity, 11007), 'asdgdwg')
		
	def tearDown(self):
		del self.entity
	
	def logPoint(self):
		currentTest = self.id().split(".")[-1]
		currentFunction = inspect.stack()[1][3]
		print ("in %s - %s" % (currentTest, currentFunction))
		

def main():
	unittest.main()

if __name__ == "__main__":
	main()