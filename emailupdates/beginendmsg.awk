{
	a[NR] = $0
}
END {
	flag1 = 0
	for (i = 1; i <= NR; i++) {
		x1 = index(a[i], "BEGINMESSAGE")
		x2 = index(a[i], "ENDMESSAGE")
		if (flag1 == 0) {
			if (x1 > 0) {
				print substr(a[i], x1 + 12)
				flag1 = 1
			}
		} else if (flag1 == 1) {
			if (x2 > 0) {
				print substr(a[i], 1, x2 - 1)
				flag1 = 2
			} else {
				print a[i]
			}
		}
	}
}
