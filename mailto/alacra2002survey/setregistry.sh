rkey="HKEY_LOCAL_MACHINE\\SOFTWARE\\XLS\\Mailto Forms\\alacra2002survey"
registry -s -k "$rkey" -n "mode" -v "advanced"
registry -s -k "$rkey" -n "required_fields" -v "first,last,company,email"
registry -s -k "$rkey" -n "web_form" -v "c:\\usr\\netscape\\server\\docs\\survey\\alacra2002survey.htm"

rkey="HKEY_LOCAL_MACHINE\\SOFTWARE\\XLS\\Mailto Forms\\alacra2002survey\\internalemail"
registry -s -k "$rkey" -n "template_files" -v "c:\\users\\xls\\src\\scripts\\mailto\\alacra2002survey\\internalemail.txt"
registry -s -k "$rkey" -n "command" -v "blat \${_file0} -f \"\${email,c}\" -t \"carolann.thomas@alacra.com,barbara.koscs@alacra.com,colin.dusaire@alacra.com,angela.lowther@alacra.com,bob.delaney@alacra.com,steven.goldstein@alacra.com,michael.angle@alacra.com\" -s \"Alacra 2002 Survey\" -q"

rkey="HKEY_LOCAL_MACHINE\\SOFTWARE\\XLS\\Mailto Forms\\alacra2002survey\\sqlinsert"
registry -s -k "$rkey" -n "template_files" -v "c:\\users\\xls\\src\\scripts\\mailto\\alacra2002survey\\alacra2002survey.sql"
registry -s -k "$rkey" -n "command" -v "isql /S data9 /U mailto /P mailto < \${_file0} > nul"

rkey="HKEY_LOCAL_MACHINE\\SOFTWARE\\XLS\\Mailto Forms\\alacra2002survey\\webresponse"
registry -s -k "$rkey" -n "template_files" -v "c:\\usr\\netscape\\server\\docs\\survey\\alacrasurveythanks.htm"
registry -s -k "$rkey" -n "command" -v "web response"

